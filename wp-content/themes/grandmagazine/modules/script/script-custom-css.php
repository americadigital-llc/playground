<?php
header('Content-type: text/css');

$pp_advance_combine_css = get_option('pp_advance_combine_css');

if(!empty($pp_advance_combine_css))
{
	//Function for compressing the CSS as tightly as possible
	function compress($buffer) {
	    //Remove CSS comments
	    $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
	    //Remove tabs, spaces, newlines, etc.
	    $buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);
	    return $buffer;
	}

	//This GZIPs the CSS for transmission to the user
	//making file size smaller and transfer rate quicker
	ob_start("ob_gzhandler");
	ob_start("compress");
}
?>

<?php
	//Check if hide portfolio navigation
	$pp_portfolio_single_nav = get_option('pp_portfolio_single_nav');
	if(empty($pp_portfolio_single_nav))
	{
?>
.portfolio_nav { display:none; }
<?php
	}
?>
<?php
	$tg_fixed_menu = kirki_get_option('tg_fixed_menu');
	
	if(!empty($tg_fixed_menu))
	{
		//Check if Wordpress admin bar is enabled
		$menu_top_value = 0;
		if(is_admin_bar_showing())
		{
			$menu_top_value = 30;
		}
?>
.top_bar.fixed
{
	position: fixed;
	animation-name: slideDown;
	-webkit-animation-name: slideDown;	
	animation-duration: 0.5s;	
	-webkit-animation-duration: 0.5s;
	z-index: 999;
	visibility: visible !important;
	top: <?php echo intval($menu_top_value); ?>px;
}

<?php
	$pp_menu_font = get_option('pp_menu_font');
	$pp_menu_font_diff = 16-$pp_menu_font;
?>
.top_bar.fixed #menu_wrapper div .nav
{
	margin-top: <?php echo intval($pp_menu_font_diff); ?>px;
}

.top_bar.fixed #searchform
{
	margin-top: <?php echo intval($pp_menu_font_diff-8); ?>px;
}

.top_bar.fixed .header_cart_wrapper
{
	margin-top: <?php echo intval($pp_menu_font_diff+5); ?>px;
}

.top_bar.fixed #menu_wrapper div .nav > li > a
{
	padding-bottom: 24px;
}

.top_bar.fixed .logo_wrapper img
{
	max-height: 40px;
	width: auto;
}
<?php
	}
	
	//Hack animation CSS for Safari
	$current_browser = grandmagazine_get_browser();

	if(isset($current_browser['name']) && $current_browser['name'] == 'Internet Explorer')
	{
?>
#wrapper
{
	overflow-x: hidden;
}
.mobile_menu_wrapper
{
    display: none;
}
body.js_nav .mobile_menu_wrapper 
{
    display: block;
}
.gallery_type, .portfolio_type
{
	opacity: 1;
}
#searchform input[type=text]
{
	width: 75%;
}
.woocommerce .logo_wrapper img
{
	max-width: 50%;
}
<?php
	}
?>

<?php
	$tg_sidemenu = kirki_get_option('tg_sidemenu');
	
	if(empty($tg_sidemenu))
	{
?>
#wrapper.menu_transparent #logo_right_button a#mobile_nav_icon
{
    display: none;
}
<?php
	}
?>

<?php
	//Check if display post info bar
	$tg_blog_display_bar = kirki_get_option('tg_blog_display_bar');
	
	if(!empty($tg_blog_display_bar))
	{
?>
body.single .footer_bar
{
	padding-bottom: 60px;
}
<?php
	}
?>

<?php
	$tg_frame = kirki_get_option('tg_frame');
	
	if(!empty($tg_frame))
	{
?>
.above_top_bar .page_content_wrapper
{
	padding: 0 12px 0 12px;
	box-sizing: border-box;
}
<?php
	}
?>

<?php
if(GRANDMAGAZINE_THEMEDEMO)
{
?>
#option_btn
{
	position: fixed;
	top: 150px;
	right: -2px;
	cursor:pointer;
	z-index: 9999;
	background: #fff;
	border-right: 0;
	width: 45px;
	height: 160px;
	text-align: center;
	border-radius: 5px 0px 0px 5px;
	box-shadow: 0 3px 15px rgba(0, 0, 0, 0.1);
}

#option_btn i
{
	font-size: 18px;
	line-height: 40px;
	color: #000;
}

#option_wrapper
{
	position: fixed;
	top: 100px;
	right:-251px;
	width: 250px;
	height: 280px;
	background: #fff;
	z-index: 99999;
	font-size: 13px;
	box-shadow: -1px 1px 10px rgba(0, 0, 0, 0.1);
	overflow: auto;
	border-radius: 5px 0px 0px 5px;
}

#option_wrapper:hover
{
	overflow-y: auto;
}

.demo_list
{
	list-style: none;
	display: block;
	margin: 30px 0 0 0;
}

.demo_list > li
{
	display: inline-block;
	position: relative;
	width: 20%;
	height: auto;
	overflow: hidden;
	cursor: pointer;
	padding: 0;
	box-sizing: border-box;
	text-align: center;
	font-size: 11px;
	margin-bottom: 15px;
}

.demo_list > li .item_content_wrapper
{1
	width: 100%;
}

.demo_list > li .item_content_wrapper .item_content
{
	width: 100%;
	box-sizing: border-box;
}

.demo_list > li .item_content_wrapper .item_content .item_thumb
{
	width: 30px;
	height: 30px;
	position: relative;
	line-height: 0;
	border-radius: 250px;
	margin: auto;
}

#option_wrapper .inner
{
	padding: 25px 15px 0 15px;
	box-sizing: border-box;
}

.demotip
{
	display: block;
}
<?php
}
?>

@media only screen and (max-width: 768px) {
	html[data-menu=leftmenu] .mobile_menu_wrapper
	{
		right: 0;
		left: initial;
		
		-webkit-transform: translate(360px, 0px);
		-ms-transform: translate(360px, 0px);
		transform: translate(360px, 0px);
		-o-transform: translate(360px, 0px);
	}
}

<?php
	$logo_margin_left = 47;
	
	//get custom logo
    $tg_retina_logo = kirki_get_option('tg_retina_logo');

    if(!empty($tg_retina_logo))
    {
    	//Get image width and height
	    $image_id = grandmagazine_get_image_id($tg_retina_logo);
	    
	    if(!empty($image_id))
	    {
	        $obj_image = wp_get_attachment_image_src($image_id, 'original');
	        
	        $image_width = 0;
		    
		    if(isset($obj_image[1]))
		    {
		    	$image_width = intval($obj_image[1]/2);
		    }
		    
		    $logo_margin_left = intval($image_width/2);
	    }
    }
?>
@media only screen and (min-width: 960px)
{
	#logo_normal.logo_container
	{
		margin-left: -<?php echo intval($logo_margin_left); ?>px;
	}
<?php
	//get custom logo
    $tg_retina_transparent_logo = kirki_get_option('tg_retina_transparent_logo');

    if(!empty($tg_retina_transparent_logo))
    {
    	//Get image width and height
	    $image_id = grandmagazine_get_image_id($tg_retina_transparent_logo);
	    
	    if(!empty($image_id))
	    {
	        $obj_image = wp_get_attachment_image_src($image_id, 'original');
	        
	        $image_width = 0;
		    
		    if(isset($obj_image[1]))
		    {
		    	$image_width = intval($obj_image[1]/2);
		    }
		    
		    $logo_margin_left = intval($image_width/2);
	    }
    }
?>
	#logo_transparent.logo_container
	{
		margin-left: -<?php echo intval($logo_margin_left); ?>px;
	}

<?php
	$tg_topbar = kirki_get_option('tg_topbar');
	
	if(!empty($tg_topbar))
	{
?>
	.logo_container
	{
		top: 45px;
	}
<?php
	}
?>

<?php
	//Check if smart sticky menu
	$tg_smart_fixed_menu = kirki_get_option('tg_smart_fixed_menu');
	
	if(!empty($tg_smart_fixed_menu))
	{
?>
@media only screen and (min-width: 960px)
{
	.top_bar.scroll
	{
		-webkit-transform: translateY(-100px);
	    -moz-transform: translateY(-100px);
	    -o-transform: translateY(-100px);
	    -ms-transform: translateY(-100px);
	    transform: translateY(-100px);
	    opacity: 0;
	}
	.top_bar.scroll.scroll_up
	{
		-webkit-transform: translateY(00px);
	    -moz-transform: translateY(0px);
	    -o-transform: translateY(0px);
	    -ms-transform: translateY(0px);
	    transform: translateY(0px);
	    opacity: 1;
	}
	.header_style_wrapper
	{
	    -webkit-transition: opacity 0.5s;
	    -moz-transition: opacity 0.5s;
	    transition: opacity 0.5s;
	}
	.header_style_wrapper.scroll_down
	{
		opacity: 0;
		z-index: 0;
	}
	
	.header_style_wrapper.scroll_up
	{
		opacity: 1;
	}
}
<?php
	}
?>

<?php
/**
*	Get custom CSS for Desktop View
**/
$pp_custom_css = get_option('pp_custom_css');


if(!empty($pp_custom_css))
{
    echo stripslashes($pp_custom_css);
}
?>

<?php
/**
*	Get custom CSS for iPad Portrait View
**/
$pp_custom_css_tablet_portrait = get_option('pp_custom_css_tablet_portrait');


if(!empty($pp_custom_css_tablet_portrait))
{
?>
@media only screen and (min-width: 768px) and (max-width: 959px) {
<?php
    echo stripslashes($pp_custom_css_tablet_portrait);
?>
}
<?php
}
?>

<?php
/**
*	Get custom CSS for iPhone Portrait View
**/
$pp_custom_css_mobile_portrait = get_option('pp_custom_css_mobile_portrait');


if(!empty($pp_custom_css_mobile_portrait))
{
?>
@media only screen and (max-width: 767px) {
<?php
    echo stripslashes($pp_custom_css_mobile_portrait);
?>
}
<?php
}
?>

<?php
/**
*	Get custom CSS for iPhone Landscape View
**/
$pp_custom_css_mobile_landscape = get_option('pp_custom_css_mobile_landscape');


if(!empty($pp_custom_css_tablet_portrait))
{
?>
@media only screen and (min-width: 480px) and (max-width: 767px) {
<?php
    echo stripslashes($pp_custom_css_mobile_landscape);
?>
}
<?php
}
?>

<?php
if(!empty($pp_advance_combine_css))
{
	ob_end_flush();
	ob_end_flush();
}
?>