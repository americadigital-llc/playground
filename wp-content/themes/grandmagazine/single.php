<?php
/**
 * The main template file for display single post page.
 *
 * @package WordPress
*/

/**
*	Get current page id
**/

$current_page_id = $post->ID;

if($post->post_type == 'galleries')
{
	//Get gallery template
	$gallery_template = get_post_meta($current_page_id, 'gallery_template', true);
	
	switch($gallery_template)
	{	
		case 'Gallery 2 Columns':
			get_template_part("gallery-2");
		break;
		
		case 'Gallery 3 Columns':
		default:
			get_template_part("gallery-3");
		break;
		
		case 'Gallery 4 Columns':
			get_template_part("gallery-4");
		break;
	}

	exit;
}
else
{
	$post_layout = get_post_meta($post->ID, 'post_layout', true);
	
	if(empty($post_layout))
	{
		$tg_blog_singlepost_layout = kirki_get_option('tg_blog_singlepost_layout');
		$post_layout = $tg_blog_singlepost_layout;
	}
	
	if (locate_template($post_layout . '.php') != '')
	{
		get_template_part($post_layout);
	}
	else
	{
		get_template_part('single-post-r-parallax');
	}
}
?>