<?php
if ( function_exists( 'add_theme_support' ) ) {
	// Setup thumbnail support
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'custom-background' );
}

if ( function_exists( 'add_image_size' ) ) { 
	add_image_size( 'grandmagazine_blog_classic', 1024, 774, true );
	add_image_size( 'grandmagazine_blog', 600, 454, true );
}

add_filter('wp_get_attachment_image_attributes', 'grandmagazine_responsive_image_fix');

function grandmagazine_responsive_image_fix($attr) {
    if (isset($attr['sizes'])) unset($attr['sizes']);
    if (isset($attr['srcset'])) unset($attr['srcset']);
    return $attr;
}

add_filter('wp_calculate_image_sizes', '__return_false', PHP_INT_MAX);
add_filter('wp_calculate_image_srcset', '__return_false', PHP_INT_MAX);
remove_filter('the_content', 'wp_make_content_images_responsive');

add_action( 'after_setup_theme', 'grandmagazine_woocommerce_support' );
function grandmagazine_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
?>