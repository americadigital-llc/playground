<?php
    $tg_blog_display_author = kirki_get_option('tg_blog_display_author');
    $author_info = get_the_author_meta('description');
    
    if($tg_blog_display_author && !empty($author_info))
    {
    	$author_name = get_the_author();
    	
    	$author_facebook = get_the_author_meta('facebook');
    	$author_twitter = get_the_author_meta('twitter');
    	$author_google = get_the_author_meta('google');
    	$author_linkedin = get_the_author_meta('linkedin');
    	$author_instagram = get_the_author_meta('instagram');
?>
<div id="about_the_author">
    <div class="gravatar"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php echo get_avatar( get_the_author_meta('email'), 200 ); ?></a></div>
    <div class="author_detail">
     	<div class="author_content">
     		<div class="author_label"><?php echo esc_html_e( 'Posted By', 'grandmagazine' ); ?></div>
     		<h4><?php echo esc_html($author_name); ?></h4>
     		<?php echo strip_tags($author_info); ?>
     		
     		<ul class="author_social">
	     		<?php
	    			if(!empty($author_facebook))
		    		{
		    	?>
		    	<li class="facebook"><a target="_blank" href="<?php echo esc_url($author_facebook); ?>"><i class="fa fa-facebook"></i></a></li>
		    	<?php
		    		}
		    	?>
		    	<?php
		    		if(!empty($author_twitter))
		    		{
		    	?>
		    	<li class="twitter"><a target="_blank" href="<?php echo esc_url($author_twitter); ?>"><i class="fa fa-twitter"></i></a></li>
		    	<?php
		    		}
		    	?>
		    	<?php
		    		if(!empty($author_google))
		    		{
		    	?>
		    	<li class="google"><a target="_blank" title="Google+" href="<?php echo esc_url($author_google); ?>"><i class="fa fa-google-plus"></i></a></li>
		    	<?php
		    		}
		    	?>
		    	<?php
		    		if(!empty($author_linkedin))
		    		{
		    	?>
		    	<li class="linkedin"><a target="_blank" title="Linkedin" href="<?php echo esc_url($author_linkedin); ?>"><i class="fa fa-linkedin"></i></a></li>
		    	<?php
		    		}
		    	?>
		    	<?php
		        	if(!empty($author_instagram))
		        	{
		        ?>
		        <li class="instagram"><a target="_blank" title="Instagram" href="<?php echo esc_url($author_instagram); ?>"><i class="fa fa-instagram"></i></a></li>
		        <?php
		        	}
		        ?>
		 	</ul>
     	</div>
    </div>
    <br class="clear"/>
</div>
<?php
    }
?>