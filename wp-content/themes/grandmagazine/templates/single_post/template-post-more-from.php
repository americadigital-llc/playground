<?php
$tg_blog_display_category_more = kirki_get_option('tg_blog_display_category_more');

//Get Post's Categories
$post_categories = wp_get_post_categories($post->ID);

if(!empty($post_categories) && !empty($tg_blog_display_category_more))
{
	$category_id = '';
	
	//Exclude from certain categories
	$tg_blog_editor_picks_cat = kirki_get_option('tg_blog_editor_picks_cat');
	$tg_blog_featured_cat = kirki_get_option('tg_blog_featured_cat');
	
	foreach($post_categories as $c)
	{
		$cat = get_category($c);
		
		if($cat->term_id != $tg_blog_editor_picks_cat && $cat->term_id != $tg_blog_featured_cat)
		{
	 		$category_id = $cat->term_id;
	 		break;
	 	}
	}
	
	$tg_blog_display_category_more_posts_items = kirki_get_option('tg_blog_display_category_more_posts_items');
	
	$single_related_posts = grandmagazine_get_single_related_posts();
	
	if(empty($single_related_posts))
	{
		$single_related_posts = array();
	}
	
	array_push($single_related_posts, $post->ID);

	$args = array(
      	  'cat' => $category_id,
      	  'post__not_in' => $single_related_posts,
      	  'showposts' => $tg_blog_display_category_more_posts_items,
      	  'orderby' => 'date',
      	  'order' => 'DESC',
      	  'suppress_filters' => false,
  	);
  	
  	$my_query = new WP_Query($args);
  	
  	if( $my_query->have_posts() ) {
 ?>
  	<h5><?php echo esc_html_e( 'More from', 'grandmagazine' ); ?> <?php echo esc_html($cat->name); ?></h5><hr/>
  	<div class="post_more">
    <?php
       while ($my_query->have_posts()) : $my_query->the_post();
       
       if(empty($single_related_posts))
	   {
			$single_related_posts = array();
		}
       
       array_push($single_related_posts, get_the_ID());
       
       $image_thumb = '';
					
		if(has_post_thumbnail(get_the_ID(), 'grandmagazine_blog'))
		{
		    $image_id = get_post_thumbnail_id(get_the_ID());
		    $image_thumb = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
		    $image_thumb = grandmagazine_filter_default_featued_image($image_thumb);
		}
    ?>
	<!-- Begin each blog post -->
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
		<div class="post_wrapper">
		    
		    <div class="post_content_wrapper">
		    
		    	<div class="post_header search">
			    	<?php
					    //Get post featured content
					    $post_content_class = 'one';
					    
					    if(!empty($image_thumb))
					    {
					        $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
					        $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
					        $post_content_class = 'two_third last';
					?>
					
					    <div class="post_img static one_third">
					      	<a href="<?php the_permalink(); ?>">
					      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
					      		
					      		<?php
						      		//Get post trending or hot status
							  		echo grandmagazine_get_status($post->ID);
							  		
						      		//Get post featured content
							  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
							  		
							  		switch($post_ft_type)
								    {
								    	case 'Vimeo Video':
								    	case 'Youtube Video':
								?>
											<div class="post_img_type_wrapper">
									    		<i class="fa fa-video-camera"></i>
									    	</div>
								<?php
								    	break;
								    	
								    	case 'Gallery':
								?>
								    		<div class="post_img_type_wrapper">
									    		<i class="fa fa-camera"></i>
									    	</div>
								<?php
								    	break;
								    	
								    } //End switch
						      	?>
						      	
						      	<?php
									//Get post view counter
									$post_view = grandmagazine_get_post_view($post->ID);
									
									if(!empty($post_view))
									{
								?>
								<div class="post_info_view">
									<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
								</div>
								<?php
									}
								?>
						      	
						     </a>
					     </div>
					
					<?php
					    }
					?>
					
					<div class="post_header_title <?php echo esc_attr($post_content_class); ?>">
				      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
				      	<div class="post_detail post_date">
				      		<span class="post_info_date">
				      			<span>
				      				<a href="<?php the_permalink(); ?>">
					      				<i class="fa fa-clock-o"></i>
					       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
				      				</a>
				      			</span>
				      		</span>
				      		<span class="post_info_author">
				      			<?php
				      				$author_name = get_the_author();
				      			?>
				      			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><span class="gravatar"><?php echo get_avatar( get_the_author_meta('email'), '60' ); ?></span><?php echo esc_html($author_name); ?></a>
				      		</span>
				      		<span class="post_info_comment">
						  		<i class="fa fa-comment-o"></i><?php echo get_comments_number($post->ID); ?>
						  	</span>
					  	</div>
					  	<?php echo grandmagazine_get_excerpt_by_id(get_the_ID(), 20); ?>
				   </div>
				</div>
				
		    </div>
		    
		</div>
	
	</div>
	<!-- End each blog post -->
    <?php
       	endwhile;
       	
       	wp_reset_postdata();
       	
       	grandmagazine_set_single_related_posts($single_related_posts);
     ?>
  	</div>
<?php
  	}
} //end if show related
?>