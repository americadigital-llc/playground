<?php
/**
 * The main template file for display page.
 *
 * @package WordPress
*/

/**
*	Get current page id
**/
$current_page_id = get_option( 'woocommerce_shop_page_id' );

get_header();

//Get Shop Sidebar
$page_sidebar = '';

//Get Shop Sidebar Display Settting
$tg_shop_layout = kirki_get_option('tg_shop_layout');

if($tg_shop_layout == 'sidebar')
{
	$page_sidebar = 'Shop Sidebar';
}

//Check if woocommerce page
$shop_page_id = get_option( 'woocommerce_shop_page_id' );

$page_title = get_the_title($shop_page_id);
	
//Get current page tagline
$page_tagline = get_post_meta($current_page_id, 'page_tagline', true);

$pp_page_bg = '';
//Get page featured image
if(has_post_thumbnail($current_page_id, 'full'))
{
    $image_id = get_post_thumbnail_id($current_page_id); 
    $image_thumb = wp_get_attachment_image_src($image_id, 'full', true);
    
    if(isset($image_thumb[0]) && !empty($image_thumb[0]))
    {
    	$pp_page_bg = $image_thumb[0];
    }
}

$grandmagazine_topbar = grandmagazine_get_topbar();
?>
<div id="page_caption" class="<?php if(!empty($pp_page_bg)) { ?>hasbg parallax <?php } ?> <?php if(!empty($grandmagazine_topbar)) { ?>withtopbar<?php } ?> " <?php if(!empty($pp_page_bg)) { ?>style="background-image:url(<?php echo esc_url($pp_page_bg); ?>);"<?php } ?>>
	
	<?php if(!empty($pp_page_bg)) { ?>
		<div class="background_overlay"></div>
	<?php } ?>
	
	<div class="page_title_wrapper">
		
		<?php if(!empty($pp_page_bg)) { ?>
			<div class="standard_wrapper">
		<?php } ?>
		
		<?php if(is_author()) { ?>
		<div class="gravatar"><?php echo get_avatar( get_the_author_meta('email'), '80' ); ?></div>
		<?php } ?>
		
	    <div class="page_title_inner">
	    	<h1 <?php if(!empty($pp_page_bg) && !empty($grandmagazine_topbar)) { ?>class ="withtopbar"<?php } ?>><?php echo esc_html($page_title); ?></h1>
	    	<?php
	        	if(!empty($page_tagline))
	        	{
	        ?>
	        	<div class="page_tagline">
	        		<?php echo wp_kses_post($page_tagline); ?>
	        	</div>
	        <?php
	        	}
	        ?>
	    </div>
	    <a href="<?php echo esc_url(home_url('/')); ?>" class="return_home"><i class="fa fa-mail-reply"></i></a>
	    
	    <?php if(!empty($pp_page_bg)) { ?>
			</div>
		<?php } ?>
	</div>
	<br class="clear"/>
</div>

<!-- Begin content -->
<div id="page_content_wrapper" <?php if(!empty($pp_page_bg)) { ?>class="hasbg"<?php } ?>>
    <div class="inner ">
    	<!-- Begin main content -->
    	<div class="inner_wrapper">
    		<div class="sidebar_content <?php if(empty($page_sidebar)) { ?>full_width<?php } else { ?>left_sidebar<?php } ?>">
				
				<?php woocommerce_content();  ?>
				
    		</div>
    		<?php if(!empty($page_sidebar)) { ?>
    		<div class="sidebar_wrapper left_sidebar">
	            <div class="sidebar">
	            
	            	<div class="content">
	            
	            		<?php 
						$page_sidebar = sanitize_title($page_sidebar);
						
						if (is_active_sidebar($page_sidebar)) { ?>
		    	    		<ul class="sidebar_widget">
		    	    		<?php dynamic_sidebar($page_sidebar); ?>
		    	    		</ul>
		    	    	<?php } ?>
	            	
	            	</div>
	        
	            </div>
            <br class="clear"/>
        
            <div class="sidebar_bottom"></div>
			</div>
    		<?php } ?>
    	</div>
    	<!-- End main content -->
    </div>
</div>
<!-- End content -->
<?php get_footer(); ?>