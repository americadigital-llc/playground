<?php
/*
Theme Name: Grand Magazine Theme
Theme URI: http://themes.themegoods.com/grandmagazine/demo
Author: ThemeGoods
Author URI: http://themeforest.net/user/ThemeGoods
License: GPLv2
*/

//Setup theme default constant and data
require_once get_template_directory() . "/lib/config.lib.php";

//Setup theme translation
require_once get_template_directory() . "/lib/translation.lib.php";

//Setup theme admin action handler
require_once get_template_directory() . "/lib/admin.action.lib.php";

//Setup theme support and image size handler
require_once get_template_directory() . "/lib/theme.support.lib.php";

//Get custom function
require_once get_template_directory() . "/lib/custom.lib.php";

//Setup menu settings
require_once get_template_directory() . "/lib/menu.lib.php";

//Setup CSS compression related functions
require_once get_template_directory() . "/lib/cssmin.lib.php";

//Setup JS compression related functions
require_once get_template_directory() . "/lib/jsmin.lib.php";

//Setup Sidebar
require_once get_template_directory() . "/lib/sidebar.lib.php";

//Setup theme custom widgets
require_once get_template_directory() . "/lib/widgets.lib.php";

//Setup auto update
require_once get_template_directory() . "/lib/theme.update.lib.php";

//Setup theme admin settings
require_once get_template_directory() . "/lib/admin.lib.php";


//Create theme admin panel
function grandmagazine_add_admin() 
{
 	$options = grandmagazine_get_options();
	
	$redirect_uri = '';
	
	if ( isset($_GET['page']) && $_GET['page'] == basename(__FILE__) ) {
	 
		if ( isset($_REQUEST['action']) && 'save' == $_REQUEST['action'] ) {
	 
			//check if verify purchase code
			if(isset($_REQUEST['pp_envato_personal_token']) && !empty($_REQUEST['pp_envato_personal_token']) && $_REQUEST['pp_envato_personal_token'] != '[ThemeGoods Activation]')
			{
				$is_verified_envato_purchase_code = false;
				require_once (get_template_directory() . "/lib/envato.lib.php");
				$obj_envato = new Envato($_REQUEST['pp_envato_personal_token']);
	
				update_option("pp_envato_personal_token", $_REQUEST['pp_envato_personal_token']);
				
				$obj_envato->set_response_type('array');
				
				$purchase_data = $obj_envato->call('/buyer/list-purchases?filter_by=wordpress-themes');
				
				if(isset($purchase_data['results']) && is_array($purchase_data['results']))
				{
					foreach($purchase_data['results'] as $result_arr)
					{
						if(isset($result_arr['item']['id']) && $result_arr['item']['id'] == ENVATOITEMID)
						{
							$is_verified_envato_purchase_code = true;
							update_option("pp_verified_envato_grandmagazine", true);
							break;
						}
					}
				}
				else if(isset($_REQUEST['pp_envato_personal_token']) && $_REQUEST['pp_envato_personal_token'] == '[ThemeGoods Activation]')
				{
					$is_verified_envato_purchase_code = true;
				}
				else
				{
					$is_verified_envato_purchase_code = false;
					delete_option("pp_verified_envato_grandmagazine", true);
				}
				
				if(!$is_verified_envato_purchase_code)
				{
					$redirect_uri.= '&action=invalid-purchase';
				}
			}
	 
			foreach ($options as $value) 
			{
				if($value['type'] != 'image' && isset($value['id']) && isset($_REQUEST[ $value['id'] ]))
				{
					update_option( $value['id'], $_REQUEST[ $value['id'] ] );
				}
			}
			
			foreach ($options as $value) {
			
				if( isset($value['id']) && isset( $_REQUEST[ $value['id'] ] )) 
				{ 
	
					if($value['id'] != GRANDMAGAZINE_SHORTNAME."_sidebar0" && $value['id'] != GRANDMAGAZINE_SHORTNAME."_ggfont0")
					{
						//if sortable type
						if(is_admin() && $value['type'] == 'sortable')
						{
							$sortable_array = serialize($_REQUEST[ $value['id'] ]);
							
							$sortable_data = $_REQUEST[ $value['id'].'_sort_data'];
							$sortable_data_arr = explode(',', $sortable_data);
							$new_sortable_data = array();
							
							foreach($sortable_data_arr as $key => $sortable_data_item)
							{
								$sortable_data_item_arr = explode('_', $sortable_data_item);
								
								if(isset($sortable_data_item_arr[0]))
								{
									$new_sortable_data[] = $sortable_data_item_arr[0];
								}
							}
							
							update_option( $value['id'], $sortable_array );
							update_option( $value['id'].'_sort_data', serialize($new_sortable_data) );
						}
						elseif(is_admin() && $value['type'] == 'font')
						{
							if(!empty($_REQUEST[ $value['id'] ]))
							{
								update_option( $value['id'], $_REQUEST[ $value['id'] ] );
								update_option( $value['id'].'_value', $_REQUEST[ $value['id'].'_value' ] );
							}
							else
							{
								delete_option( $value['id'] );
								delete_option( $value['id'].'_value' );
							}
						}
						elseif(is_admin())
						{
							if($value['type']=='image')
							{
								update_option( $value['id'], esc_url($_REQUEST[ $value['id'] ])  );
							}
							elseif($value['type']=='textarea')
							{
								if(isset($value['validation']) && !empty($value['validation']))
								{
									update_option( $value['id'], esc_textarea($_REQUEST[ $value['id'] ]) );
								}
								else
								{
									update_option( $value['id'], $_REQUEST[ $value['id'] ] );
								}
							}
							elseif($value['type']=='iphone_checkboxes' OR $value['type']=='jslider')
							{
								update_option( $value['id'], $_REQUEST[ $value['id'] ]  );
							}
							else
							{
								if(isset($value['validation']) && !empty($value['validation']))
								{
									$request_value = $_REQUEST[ $value['id'] ];
									
									//Begin data validation
									switch($value['validation'])
									{
										case 'text':
										default:
											$request_value = sanitize_text_field($request_value);
										
										break;
										
										case 'email':
											$request_value = sanitize_email($request_value);
	
										break;
										
										case 'javascript':
											$request_value = sanitize_text_field($request_value);
	
										break;
										
									}
									update_option( $value['id'], $request_value);
								}
								else
								{
									update_option( $value['id'], $_REQUEST[ $value['id'] ]  );
								}
							}
						}
					}
					elseif(is_admin() && isset($_REQUEST[ $value['id'] ]) && !empty($_REQUEST[ $value['id'] ]))
					{
						if($value['id'] == GRANDMAGAZINE_SHORTNAME."_sidebar0")
						{
							//get last sidebar serialize array
							$current_sidebar = get_option(GRANDMAGAZINE_SHORTNAME."_sidebar");
							$request_value = $_REQUEST[ $value['id'] ];
							$request_value = sanitize_text_field($request_value);
							
							$current_sidebar[ $request_value ] = $request_value;
				
							update_option( GRANDMAGAZINE_SHORTNAME."_sidebar", $current_sidebar );
						}
						elseif($value['id'] == GRANDMAGAZINE_SHORTNAME."_ggfont0")
						{
							//get last ggfonts serialize array
							$current_ggfont = get_option(GRANDMAGAZINE_SHORTNAME."_ggfont");
							$current_ggfont[ $_REQUEST[ $value['id'] ] ] = $_REQUEST[ $value['id'] ];
				
							update_option( GRANDMAGAZINE_SHORTNAME."_ggfont", $current_ggfont );
						}
					}
				} 
				else 
				{ 
					if(is_admin() && isset($value['id']))
					{
						delete_option( $value['id'] );
					}
				} 
			}
	
			header("Location: admin.php?page=functions.php&saved=true".$redirect_uri.$_REQUEST['current_tab']);
		}  
	} 
	 
	add_theme_page('Theme Setting', 'Theme Setting', 'administrator', basename(__FILE__), 'grandmagazine_admin', '');
	}
	
	function grandmagazine_fonts_url() 
	{
		//Get all Google Web font CSS
		$grandmagazine_google_fonts = grandmagazine_get_google_fonts();
		
		$tg_fonts_family = array();
		if(is_array($grandmagazine_google_fonts) && !empty($grandmagazine_google_fonts))
		{
			foreach($grandmagazine_google_fonts as $tg_font)
			{
				$tg_fonts_family[] = kirki_get_option($tg_font);
			}
		}
	
		$tg_fonts_family = array_unique($tg_fonts_family);
		$font_families = array();
	
		foreach($tg_fonts_family as $key => $tg_google_font)
		{	    
		    if(!empty($tg_google_font))
		    {
		    	$font_families[] = $tg_google_font.':300,400,500,600,700';
		    }
		}
		
		$query_args = array(
		    'family' => urlencode( implode( '|', $font_families ) ),
		    'subset' => urlencode( 'latin,cyrillic-ext,greek-ext,cyrillic' ),
		);
		
		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	 
	    return esc_url_raw( $fonts_url );
	}
	
	function grandmagazine_enqueue_admin_page_scripts() 
	{
		$current_screen = grandmagazine_get_current_screen();
		
		//Register theme admin CSS
		wp_enqueue_style(array('thickbox'));
		wp_enqueue_style("fontawesome", get_template_directory_uri()."/css/font-awesome.min.css", false, "", "all");
		wp_enqueue_style('switchery', get_template_directory_uri().'/css/switchery.css', false, GRANDMAGAZINE_THEMEVERSION, 'all');
		wp_enqueue_style("tooltipster", get_template_directory_uri()."/css/tooltipster.css", false, GRANDMAGAZINE_THEMEVERSION, "all");
		
		if(isset($current_screen->base) && $current_screen->base == 'appearance_page_functions')
		{
			wp_enqueue_style("codemirror", get_template_directory_uri()."/css/codemirror.css", false, GRANDMAGAZINE_THEMEVERSION, "all");
		}
		
		wp_enqueue_style("grandmagazine-functions", get_template_directory_uri()."/functions/functions.css", false, GRANDMAGAZINE_THEMEVERSION, "all");
		
		if(property_exists($current_screen, 'post_type') && ($current_screen->post_type == 'page'))
		{
			wp_enqueue_style("grandmagazine-jqueryui", get_template_directory_uri()."/css/jqueryui/custom.css", false, GRANDMAGAZINE_THEMEVERSION, "all");
		}
		
		//Register theme admin javascript
		wp_enqueue_script(array('jquery-ui-core', 'jquery-ui-sortable', 'media-upload', 'thickbox'));
		wp_enqueue_script('switchery', get_template_directory_uri().'/functions/switchery.js', false, GRANDMAGAZINE_THEMEVERSION);
		wp_enqueue_script('tooltipster', get_template_directory_uri().'/js/jquery.tooltipster.min.js', false, GRANDMAGAZINE_THEMEVERSION);
		
		if(isset($current_screen->base) && $current_screen->base == 'appearance_page_functions')
		{
			wp_enqueue_script('codemirror', get_template_directory_uri().'/functions/codemirror.js', false, GRANDMAGAZINE_THEMEVERSION);
			wp_enqueue_script('codemirror-css', get_template_directory_uri().'/functions/css.js', false, GRANDMAGAZINE_THEMEVERSION);
		}
		
		wp_register_script( "grandmagazine-theme-cript", get_template_directory_uri()."/functions/theme_script.js", false, GRANDMAGAZINE_THEMEVERSION, true);
		$params = array(
		  'ajaxurl' => esc_url(admin_url('admin-ajax.php')),
		);
		wp_localize_script( 'grandmagazine-theme-cript', 'tgAjax', $params );
		wp_enqueue_script( 'grandmagazine-theme-cript' );
	
	}
	
	add_action('admin_enqueue_scripts',	'grandmagazine_enqueue_admin_page_scripts' );
	
	function grandmagazine_enqueue_front_page_scripts() 
	{
	    wp_enqueue_style("grandmagazine-reset-css", get_template_directory_uri()."/css/reset.css", false, "");
		wp_enqueue_style("grandmagazine-wordpress-css", get_template_directory_uri()."/css/wordpress.css", false, "");
		wp_enqueue_style("grandmagazine-animation", get_template_directory_uri()."/css/animation.css", false, "", "all");
		wp_enqueue_style("magnific", get_template_directory_uri()."/css/magnific-popup.css", false, "", "all");
		wp_enqueue_style("grandmagazine-jquery-ui-css", get_template_directory_uri()."/css/jqueryui/custom.css", false, "");
		wp_enqueue_style("flexslider", get_template_directory_uri()."/js/flexslider/flexslider.css", false, "", "all");
		wp_enqueue_style("tooltipster", get_template_directory_uri()."/css/tooltipster.css", false, "", "all");
		wp_enqueue_style("grandmagazine-screen-css", get_template_directory_uri().'/css/screen.css', false, "", "all");
		
		//Add Google Font
		wp_enqueue_style( 'grandmagazine-fonts', grandmagazine_fonts_url(), array(), null );
		
		//Add Font Awesome Support
		wp_enqueue_style("fontawesome", get_template_directory_uri()."/css/font-awesome.min.css", false, "", "all");
		
		//Add custom CSS on theme admin support
		wp_enqueue_style("grandmagazine-script-custom-css", admin_url('admin-ajax.php')."?action=grandmagazine_script_custom_css", false, "", "all");
		
		$tg_boxed = kirki_get_option('tg_boxed');
	    if(GRANDMAGAZINE_THEMEDEMO && isset($_GET['boxed']) && !empty($_GET['boxed']))
	    {
	    	$tg_boxed = 1;
	    }
	    
	    //If enable boxed layout
	    if(!empty($tg_boxed))
	    {
	    	wp_enqueue_style("grandmagazine-boxed-css", get_template_directory_uri().'/css/boxed.css', false, "", "all");
	    }
		
		//If using child theme
		if(is_child_theme())
		{
		    wp_enqueue_style('grandmagazine-childtheme-css', get_stylesheet_directory_uri()."/style.css", false, "", "all");
		}
		
		//Enqueue javascripts
		wp_enqueue_script(array('jquery'));
		
		$js_path = get_template_directory()."/js/";
		$js_arr = array(
			'magnific-popup'				=> 'jquery.magnific-popup.js',
			'easing'						=> 'jquery.easing.js',
		    'waypoints'						=> 'waypoints.min.js',
		    'masory'						=> 'jquery.masory.js',
		    'tooltipster'					=> 'jquery.tooltipster.min.js',
		    'jarallax'						=> 'jarallax.js',
		    'sticky-kit'					=> 'jquery.sticky-kit.min.js',
		    'grandmagazine-custom-plugins'	=> 'custom_plugins.js',
			'grandmagazine-custom-script' 	=> 'custom.js',
		);
		$js = "";
	
		foreach($js_arr as $key => $file) {
			if($file != 'jquery.js' && $file != 'jquery-ui.js')
			{
				wp_enqueue_script($key, get_template_directory_uri()."/js/".$file, false, "", true);
			}
		}
		
		//If display photostream
		$pp_photostream = get_option('pp_photostream');
	
		if(!empty($pp_photostream))
		{
			wp_enqueue_script("modernizr", get_template_directory_uri()."/js/modernizr.js", false, GRANDMAGAZINE_THEMEVERSION, true);
			wp_enqueue_script("gridrotator", get_template_directory_uri()."/js/jquery.gridrotator.js", false, GRANDMAGAZINE_THEMEVERSION, true);
			wp_enqueue_script("grandmagazine-script-footer-gridrotator", admin_url('admin-ajax.php')."?action=grandmagazine_script_gridrotator&amp;grid=footer_photostream&amp;rows=1", false, GRANDMAGAZINE_THEMEVERSION, true);
		}
	}
	add_action( 'wp_enqueue_scripts', 'grandmagazine_enqueue_front_page_scripts' );
	
	
	//Enqueue mobile CSS after all others CSS load
	function grandmagazine_register_mobile_css() {
		//Check if enable responsive layout
		$tg_mobile_responsive = kirki_get_option('tg_mobile_responsive');
		
		if(!empty($tg_mobile_responsive))
		{
			wp_enqueue_style('grandmagazine-script-responsive-css', get_template_directory_uri()."/css/grid.css", false, "", "all");
		}
	}
	add_action('wp_enqueue_scripts', 'grandmagazine_register_mobile_css', 15);
	
	
	function grandmagazine_admin() {
	 
	$grandmagazine_options = grandmagazine_get_options();
	$i=0;
	
	$pp_font_family = get_option('pp_font_family');
	
	if(function_exists( 'wp_enqueue_media' )){
	    wp_enqueue_media();
	}
	?>
		
		<div id="pp_loading"><span><?php esc_html_e( 'Updating...', 'grandmagazine' ); ?></span></div>
		<div id="pp_success"><span><?php esc_html_e( 'Successfully Update', 'grandmagazine' ); ?></span></div>
		
		<form id="pp_form" method="post" enctype="multipart/form-data">
		<div class="pp_wrap rm_wrap">
		
		<div class="header_wrap">
			<div style="float:left">
			<?php
				//Display logo in theme setting
				$tg_retina_logo_for_admin = kirki_get_option('tg_retina_logo_for_admin');
				$tg_retina_logo = kirki_get_option('tg_retina_logo');
				
				if(empty($tg_retina_logo_for_admin))
				{
			?>
			<h2><?php esc_html_e('Theme Setting', 'grandmagazine' ); ?><span class="pp_version"><?php esc_html_e('Version', 'grandmagazine' ); ?> <?php echo GRANDMAGAZINE_THEMEVERSION; ?></span></h2>
			<?php
				}
				else if(!empty($tg_retina_logo))
				{
			?>
			<div class="pp_setting_logo_wrapper">
			<?php
					//Get image width and height
			    	$image_id = grandmagazine_get_image_id($tg_retina_logo);
			    	if(!empty($image_id))
			    	{
			    		$obj_image = wp_get_attachment_image_src($image_id, 'original');
			    		
			    		$image_width = 0;
				    	$image_height = 0;
				    	
				    	if(isset($obj_image[1]))
				    	{
				    		$image_width = intval($obj_image[1]/2);
				    	}
				    	if(isset($obj_image[2]))
				    	{
				    		$image_height = intval($obj_image[2]/2);
				    	}
			    	}
			    	else
			    	{
				    	$image_width = 0;
				    	$image_height = 0;
			    	}
						
					if($image_width > 0 && $image_height > 0)
					{
					?>
					<img src="<?php echo esc_url($tg_retina_logo); ?>" alt="<?php esc_attr(get_bloginfo('name')); ?>" width="<?php echo esc_attr($image_width); ?>" height="<?php echo esc_attr($image_height); ?>"/>
					<?php
					}
					else
					{
					?>
	    	    	<img src="<?php echo esc_url($tg_retina_logo); ?>" alt="<?php esc_attr(get_bloginfo('name')); ?>" width="94" height="44"/>
	    	    <?php 
		    	    }
		    	?>
		    	<span class="pp_version"><?php esc_html_e('Version', 'grandmagazine' ); ?> <?php echo GRANDMAGAZINE_THEMEVERSION; ?></span>
			</div>
			<?php
				}
			?>
			</div>
			<div style="float:right;margin:32px 0 0 0">
				<input id="save_ppsettings" name="save_ppsettings" class="button button-primary button-large" type="submit" value="<?php esc_html_e('Save', 'grandmagazine' ); ?>" />
				<br/><br/>
				<input type="hidden" name="action" value="save" />
				<input type="hidden" name="current_tab" id="current_tab" value="#pp_panel_home" />
				<input type="hidden" name="pp_save_skin_flg" id="pp_save_skin_flg" value="" />
				<input type="hidden" name="pp_save_skin_name" id="pp_save_skin_name" value="" />
			</div>
			<input type="hidden" name="pp_admin_url" id="pp_admin_url" value="<?php echo get_template_directory_uri(); ?>"/>
			<br style="clear:both"/>
	
	<?php
		//Check if theme has new update
	?>
	
		</div>
		
		<div class="pp_wrap">
		<div id="pp_panel">
		<?php 
			foreach ($grandmagazine_options as $value) {
				
				$active = '';
				
				if($value['type'] == 'section')
				{
					if($value['name'] == 'Home')
					{
						$active = 'nav-tab-active';
					}
					echo '<a id="pp_panel_'.strtolower($value['name']).'_a" href="#pp_panel_'.strtolower($value['name']).'" class="nav-tab '.$active.'"><span class="dashicons  '.$value['icon'].'"></span>'.str_replace('-', ' ', $value['name']).'</a>';
				}
			}
		?>
		</h2>
		</div>
	
		<div class="rm_opts">
		
	<?php 
	$url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
	
	foreach ($grandmagazine_options as $value) {
	switch ( $value['type'] ) {
	 
	case "open":
	?> <?php break;
	 
	case "close":
	?>
		
		</div>
		</div>
	
	
		<?php break;
	 
	case "title":
	?>
		<br />
	
	
	<?php break;
	 
	case 'text':
		
		//if sidebar input then not show default value
		if($value['id'] != GRANDMAGAZINE_SHORTNAME."_sidebar0" && $value['id'] != GRANDMAGAZINE_SHORTNAME."_ggfont0")
		{
			$default_val = get_option( $value['id'] );
		}
		else
		{
			$default_val = '';	
		}
	?>
	
		<div id="<?php echo esc_attr($value['id']); ?>_section" class="rm_input rm_text"><label for="<?php echo esc_attr($value['id']); ?>"><?php echo stripslashes($value['name']); ?></label>
		
		<small class="description"><?php echo stripslashes($value['desc']); ?></small>
		
		<input name="<?php echo esc_attr($value['id']); ?>"
			id="<?php echo esc_attr($value['id']); ?>" type="<?php echo esc_attr($value['type']); ?>"
			value="<?php if ($default_val != "") { echo esc_attr(get_option( $value['id'])) ; } else { echo esc_attr($value['std']); } ?>"
			<?php if(!empty($value['size'])) { echo 'style="width:'.intval($value['size']).'"'; } ?> />
		<div class="clearfix"></div>
		
		<?php
		if($value['id'] == GRANDMAGAZINE_SHORTNAME."_sidebar0")
		{
			$current_sidebar = get_option(GRANDMAGAZINE_SHORTNAME."_sidebar");
			
			if(!empty($current_sidebar))
			{
		?>
			<br class="clear"/><br/>
		 	<div class="pp_sortable_wrapper">
			<ul id="current_sidebar" class="rm_list">
	
		<?php
			foreach($current_sidebar as $sidebar)
			{
		?> 
				
				<li id="<?php echo esc_attr($sidebar); ?>"><div class="title"><?php echo esc_html($sidebar); ?></div><a href="<?php echo esc_url($url); ?>" class="sidebar_del" rel="<?php echo esc_attr($sidebar); ?>"><span class="dashicons dashicons-no"></span></a><br style="clear:both"/></li>
		
		<?php
			}
		?>
		
			</ul>
			</div>
			<br style="clear:both"/>
		<?php
			}
		}
		?>
	
		</div>
		<?php
	break;
	
	case 'image':
	case 'music':
	?>
	
		<div id="<?php echo esc_attr($value['id']); ?>_section" class="rm_input rm_text"><label for="<?php echo esc_attr($value['id']); ?>"><?php echo stripslashes($value['name']); ?></label><br/>
		<input id="<?php echo esc_attr($value['id']); ?>" type="text" name="<?php echo esc_attr($value['id']); ?>" value="<?php echo get_option($value['id']); ?>" style="width:200px" class="upload_text" readonly />
		<input id="<?php echo esc_attr($value['id']); ?>_button" name="<?php echo esc_attr($value['id']); ?>_button" type="button" value="Browse" class="upload_btn button" rel="<?php echo esc_attr($value['id']); ?>" style="margin:5px 0 0 5px" />
		<small class="description"><?php echo stripslashes($value['desc']); ?></small>
		<div class="clearfix"></div>
		
		<script>
		jQuery(document).ready(function() {
			"use strict";
			jQuery('#<?php echo esc_js($value['id']); ?>_button').click(function() {
	         	var send_attachment_bkp = wp.media.editor.send.attachment;
			    wp.media.editor.send.attachment = function(props, attachment) {
			    	formfield = jQuery('#<?php echo esc_js($value['id']); ?>').attr('name');
		         	jQuery('#'+formfield).attr('value', attachment.url);
			
			        wp.media.editor.send.attachment = send_attachment_bkp;
			    }
			
			    wp.media.editor.open();
	        });
	    });
		</script>
		
		<?php 
			$current_value = get_option( $value['id'] );
			
			if(!is_bool($current_value) && !empty($current_value))
			{
				$url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
			
				if($value['type']=='image')
				{
		?>
		
			<div id="<?php echo esc_attr($value['id']); ?>_wrapper" style="width:380px;font-size:11px;"><br/>
				<img src="<?php echo get_option($value['id']); ?>" style="max-width:500px"/><br/><br/>
				<a href="<?php echo esc_url($url); ?>" class="image_del button" rel="<?php echo esc_attr($value['id']); ?>"><?php esc_html_e('Delete', 'grandmagazine' ); ?></a>
			</div>
			<?php
				}
				else
				{
			?>
			<div id="<?php echo esc_attr($value['id']); ?>_wrapper" style="width:380px;font-size:11px;">
				<br/><a href="<?php echo get_option( $value['id'] ); ?>">
				<?php esc_html_e('Listen current music', 'grandmagazine' ); ?></a>&nbsp;<a href="<?php echo esc_url($url); ?>" class="image_del button" rel="<?php echo esc_attr($value['id']); ?>"><?php esc_html_e('Delete', 'grandmagazine' ); ?></a>
			</div>
		<?php
				}
			}
		?>
	
		</div>
		<?php
	break;
	
	case 'colorpicker':
	?>
		<div id="<?php echo esc_attr($value['id']); ?>_section" class="rm_input rm_text"><label for="<?php echo esc_attr($value['id']); ?>"><?php echo stripslashes($value['name']); ?></label><br/>
		<input name="<?php echo esc_attr($value['id']); ?>"
			id="<?php echo esc_attr($value['id']); ?>" type="text" 
			value="<?php if ( get_option( $value['id'] ) != "" ) { echo stripslashes(get_option( $value['id'])  ); } else { echo esc_attr($value['std']); } ?>"
			<?php if(!empty($value['size'])) { echo 'style="width:'.$value['size'].'"'; } ?>  class="color_picker" readonly/>
		<div id="<?php echo esc_attr($value['id']); ?>_bg" class="colorpicker_bg" onclick="jQuery('#<?php echo esc_js($value['id']); ?>').click()" style="background:<?php if (get_option( $value['id'] ) != "") { echo stripslashes(get_option( $value['id'])  ); } else { echo esc_attr($value['std']); } ?> url(<?php echo get_template_directory_uri(); ?>/functions/images/trigger.png) center no-repeat;">&nbsp;</div>
			<small class="description"><?php echo stripslashes($value['desc']); ?></small>
		<div class="clearfix"></div>
		
		</div>
		
	<?php
	break;
	 
	case 'textarea':
	?>
	
		<div id="<?php echo esc_attr($value['id']); ?>_section" class="rm_input rm_textarea"><label
			for="<?php echo esc_attr($value['id']); ?>"><?php echo stripslashes($value['name']); ?></label>
			
		<small class="description"><?php echo stripslashes($value['desc']); ?></small>
		
		<textarea id="<?php echo esc_attr($value['id']); ?>" name="<?php echo esc_attr($value['id']); ?>"
			type="<?php echo esc_attr($value['type']); ?>" cols="" rows=""><?php if ( get_option( $value['id'] ) != "") { echo stripslashes(get_option( $value['id']) ); } else { echo esc_html($value['std']); } ?></textarea>
		
		<div class="clearfix"></div>
	
		</div>
	
		<?php
	break;
	
	case 'css':
	?>
	
		<div id="<?php echo esc_attr($value['id']); ?>_section" class="rm_input rm_textarea"><label
			for="<?php echo esc_attr($value['id']); ?>"><?php echo stripslashes($value['name']); ?></label>
			
		<small class="description"><?php echo stripslashes($value['desc']); ?></small>
		
		<textarea id="<?php echo esc_attr($value['id']); ?>" class="css" name="<?php echo esc_attr($value['id']); ?>"
			type="<?php echo esc_attr($value['type']); ?>"><?php if ( get_option( $value['id'] ) != "") { echo stripslashes(get_option( $value['id']) ); } else { echo esc_html($value['std']); } ?></textarea>
		
		<div class="clearfix"></div>
	
		</div>
	
		<?php
	break;
	 
	case 'select':
	?>
	
		<div id="<?php echo esc_attr($value['id']); ?>_section" class="rm_input rm_select"><label
			for="<?php echo esc_attr($value['id']); ?>"><?php echo stripslashes($value['name']); ?></label><br/>
	
		<select name="<?php echo esc_attr($value['id']); ?>"
			id="<?php echo esc_attr($value['id']); ?>">
			<?php foreach ($value['options'] as $key => $option) { ?>
			<option
			<?php if (get_option( $value['id'] ) == $key) { echo 'selected="selected"'; } ?>
				value="<?php echo esc_attr($key); ?>"><?php echo esc_html($option); ?></option>
			<?php } ?>
		</select> <small class="description"><?php echo stripslashes($value['desc']); ?></small>
		<div class="clearfix"></div>
		</div>
		<?php
	break;
	
	case 'font':
	?>
	
		<div id="<?php echo esc_attr($value['id']); ?>_section" class="rm_input rm_font"><label
			for="<?php echo esc_attr($value['id']); ?>"><?php echo stripslashes($value['name']); ?></label><br/>
	
		<div id="<?php echo esc_attr($value['id']); ?>_wrapper" style="float:left;font-size:11px;">
		<select class="pp_font" data-sample="<?php echo esc_attr($value['id']); ?>_sample" data-value="<?php echo esc_attr($value['id']); ?>_value" name="<?php echo esc_attr($value['id']); ?>"
			id="<?php echo esc_attr($value['id']); ?>">
			<option value="" data-family="">---- <?php esc_html_e('Theme Default Font', 'grandmagazine' ); ?> ----</option>
			<?php 
				foreach ($pp_font_arr as $key => $option) { ?>
			<option
			<?php if (get_option( $value['id'] ) == $option['css-name']) { echo 'selected="selected"'; } ?>
				value="<?php echo esc_attr($option['css-name']); ?>" data-family="<?php echo esc_attr($option['font-name']); ?>"><?php echo esc_html($option['font-name']); ?></option>
			<?php } ?>
		</select> 
		<input type="hidden" id="<?php echo esc_attr($value['id']); ?>_value" name="<?php echo esc_attr($value['id']); ?>_value" value="<?php echo get_option( $value['id'].'_value' ); ?>"/>
		<br/><br/><div id="<?php echo esc_attr($value['id']); ?>_sample" class="pp_sample_text"><?php esc_html_e('Sample Text', 'grandmagazine' ); ?></div>
		</div>
		<small class="description"><?php echo stripslashes($value['desc']); ?></small>
		<div class="clearfix"></div>
		</div>
		<?php
	break;
	 
	case 'radio':
	?>
	
		<div id="<?php echo esc_attr($value['id']); ?>_section" class="rm_input rm_select"><label
			for="<?php echo esc_attr($value['id']); ?>"><?php echo stripslashes($value['name']); ?></label><br/><br/>
	
		<div style="margin-top:5px;float:left;<?php if(!empty($value['desc'])) { ?>width:300px<?php } else { ?>width:500px<?php } ?>">
		<?php foreach ($value['options'] as $key => $option) { ?>
		<div style="float:left;<?php if(!empty($value['desc'])) { ?>margin:0 20px 20px 0<?php } ?>">
			<input style="float:left;" id="<?php echo esc_attr($value['id']); ?>" name="<?php echo esc_attr($value['id']); ?>" type="radio"
			<?php if (get_option( $value['id'] ) == $key) { echo 'checked="checked"'; } ?>
				value="<?php echo esc_attr($key); ?>"/><?php echo esc_html($option); ?>
		</div>
		<?php } ?>
		</div>
		
		<?php if(!empty($value['desc'])) { ?>
			<small class="description"><?php echo stripslashes($value['desc']); ?></small>
		<?php } ?>
		<div class="clearfix"></div>
		</div>
		<?php
	break;
	
	case 'sortable':
	?>
	
		<div id="<?php echo esc_attr($value['id']); ?>_section" class="rm_input rm_select"><label
			for="<?php echo esc_attr($value['id']); ?>"><?php echo stripslashes($value['name']); ?></label><br/>
	
		<div style="float:left;width:100%;">
		<?php 
		$sortable_array = array();
		if(get_option( $value['id'] ) != 1)
		{
			$sortable_array = unserialize(get_option( $value['id'] ));
		}
		
		$current = 1;
		
		if(!empty($value['options']))
		{
		?>
		<select name="<?php echo esc_attr($value['id']); ?>"
			id="<?php echo esc_attr($value['id']); ?>" class="pp_sortable_select">
		<?php
		foreach ($value['options'] as $key => $option) { 
			if($key > 0)
			{
		?>
		<option value="<?php echo esc_attr($key); ?>" data-rel="<?php echo esc_attr($value['id']); ?>_sort" title="<?php echo html_entity_decode($option); ?>"><?php echo html_entity_decode($option); ?></option>
		<?php }
		
				if($current>1 && ($current-1)%3 == 0)
				{
		?>
		
				<br style="clear:both"/>
		
		<?php		
				}
				
				$current++;
			}
		?>
		</select>
		<a class="button pp_sortable_button" data-rel="<?php echo esc_attr($value['id']); ?>" class="button" style="display:inline-block"><?php echo esc_html__('Add', 'grandmagazine' ); ?></a>
		<?php
		}
		?>
		 
		 <br style="clear:both"/><br/>
		 
		 <div class="pp_sortable_wrapper">
		 <ul id="<?php echo esc_attr($value['id']); ?>_sort" class="pp_sortable" rel="<?php echo esc_attr($value['id']); ?>_sort_data"> 
		 <?php
		 	$sortable_data_array = unserialize(get_option( $value['id'].'_sort_data' ));
	
		 	if(!empty($sortable_data_array))
		 	{
		 		foreach($sortable_data_array as $key => $sortable_data_item)
		 		{
			 		if(!empty($sortable_data_item))
			 		{
		 		
		 ?>
		 		<li id="<?php echo esc_attr($sortable_data_item); ?>_sort" class="ui-state-default"><div class="title"><?php echo esc_html($value['options'][$sortable_data_item]); ?></div><a data-rel="<?php echo esc_attr($value['id']); ?>_sort" href="javascript:;" class="remove"><span class="dashicons dashicons-no"></span></a><br style="clear:both"/></li> 	
		 <?php
		 			}
		 		}
		 	}
		 ?>
		 </ul>
		 
		 </div>
		 
		</div>
		
		<input type="hidden" id="<?php echo esc_attr($value['id']); ?>_sort_data" name="<?php echo esc_attr($value['id']); ?>_sort_data" value="" style="width:100%"/>
		<br style="clear:both"/><br/>
		
		<div class="clearfix"></div>
		</div>
		<?php
	break;
	 
	case "checkbox":
	?>
	
		<div id="<?php echo esc_attr($value['id']); ?>_section" class="rm_input rm_checkbox"><label
			for="<?php echo esc_attr($value['id']); ?>"><?php echo stripslashes($value['name']); ?></label><br/>
	
		<?php if(get_option($value['id'])){ $checked = "checked=\"checked\""; }else{ $checked = "";} ?>
		<input type="checkbox" name="<?php echo esc_attr($value['id']); ?>"
			id="<?php echo esc_attr($value['id']); ?>" value="true" <?php echo esc_html($checked); ?> />
	
	
		<small class="description"><?php echo stripslashes($value['desc']); ?></small>
		<div class="clearfix"></div>
		</div>
	<?php break; 
	
	case "iphone_checkboxes":
	?>
	
		<div id="<?php echo esc_attr($value['id']); ?>_section" class="rm_input rm_checkbox"><label
			for="<?php echo esc_attr($value['id']); ?>"><?php echo stripslashes($value['name']); ?></label>
	
		<small class="description"><?php echo stripslashes($value['desc']); ?></small>
	
		<?php if(get_option($value['id'])){ $checked = "checked=\"checked\""; }else{ $checked = "";} ?>
		<input type="checkbox" class="iphone_checkboxes" name="<?php echo esc_attr($value['id']); ?>"
			id="<?php echo esc_attr($value['id']); ?>" value="true" <?php echo esc_html($checked); ?> />
	
		<div class="clearfix"></div>
		</div>
	
	<?php break; 
	
	case "html":
	?>
	
		<div id="<?php echo esc_attr($value['id']); ?>_section" class="rm_input rm_checkbox"><label
			for="<?php echo esc_attr($value['id']); ?>"><?php echo stripslashes($value['name']); ?></label><br/>
	
		<small class="description"><?php echo stripslashes($value['desc']); ?></small>
	
		<?php echo stripslashes($value['html']); ?>
	
		<div class="clearfix"></div>
		</div>
	
	<?php break; 
	
	case "shortcut":
	?>
	
		<div id="<?php echo esc_attr($value['id']); ?>_section" class="rm_input rm_shortcut">
	
		<ul class="pp_shortcut_wrapper">
		<?php 
			$count_shortcut = 1;
			foreach ($value['options'] as $key_shortcut => $option) { ?>
			<li><a href="#<?php echo esc_attr($key_shortcut); ?>" <?php if($count_shortcut==1) { ?>class="active"<?php } ?>><?php echo esc_html($option); ?></a></li>
		<?php $count_shortcut++; } ?>
		</ul>
	
		<div class="clearfix"></div>
		</div>
	
	<?php break; 
		
	case "section":
	
	$i++;
	
	?>
	
		<div id="pp_panel_<?php echo strtolower($value['name']); ?>" class="rm_section">
		<div class="rm_title">
		<h3><?php echo stripslashes($value['name']); ?></h3>
		<span class="submit"><input class="button-primary" name="save<?php echo esc_attr($i); ?>" type="submit"
			value="Save changes" /> </span>
		<div class="clearfix"></div>
		</div>
		<div class="rm_options"><?php break;
	 
	}
	}
	?>
	 	
	 	<div class="clearfix"></div>
	 	</form>
	 	</div>
	</div>

<?php
}

add_action('admin_menu', 'grandmagazine_add_admin');

/**
*	End Theme Setting Panel
**/ 


//Setup theme custom filters
require_once get_template_directory() . "/lib/theme.filter.lib.php";

//Setup required plugin activation
require_once get_template_directory() . "/lib/tgm.lib.php";

//Setup Theme Customizer
require_once get_template_directory() . "/modules/kirki/kirki.php";
require_once get_template_directory() . "/lib/customizer.lib.php";

//Setup page custom fields and action handler
require_once get_template_directory() . "/fields/page.fields.php";

// Setup shortcode generator
require_once get_template_directory() . "/modules/shortcode_generator.php";

//Check if Woocommerce is installed	
if(class_exists('Woocommerce'))
{
	//Setup Woocommerce Config
	require_once get_template_directory() . "/modules/woocommerce.php";
}

/**
*	Setup one click importer function
**/
/*add_action('wp_ajax_grandmagazine_import_demo_content', 'grandmagazine_import_demo_content');
add_action('wp_ajax_nopriv_grandmagazine_import_demo_content', 'grandmagazine_import_demo_content');

function grandmagazine_import_demo_content() {
	if(is_admin() && isset($_POST['demo']) && !empty($_POST['demo']))
	{
	    if ( !defined('WP_LOAD_IMPORTERS') ) define('WP_LOAD_IMPORTERS', true);
	    
	    // Load Importer API
	    require_once ABSPATH . 'wp-admin/includes/import.php';
	
	    if ( ! class_exists( 'WP_Importer' ) ) {
	        $class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
	        if ( file_exists( $class_wp_importer ) )
	        {
	            require_once $class_wp_importer;
	        }
	    }
	
	    if ( ! class_exists( 'WP_Import' ) ) {
	    	$class_wp_importer = get_template_directory() ."/modules/import/wordpress-importer.php";

	        if ( file_exists( $class_wp_importer ) )
	            require_once $class_wp_importer;
	    }
	
	    $import_files = array();
	    $page_on_front ='';
	    
	    //Create empty menu first before importing
	    $footer_menu_exists = wp_get_nav_menu_object('Footer Menu');
	    if(!$footer_menu_exists)
	    {
		    $footer_menu_id = wp_create_nav_menu('Footer Menu');
	    }
	    
	    $primary_menu_exists = wp_get_nav_menu_object('Main Menu');
	    if(!$primary_menu_exists)
	    {
		    $primary_menu_id = wp_create_nav_menu('Main Menu');
	    }
	    
	    $side_mobile_menu_exists = wp_get_nav_menu_object('Side Mobile Menu');
	    if(!$side_mobile_menu_exists)
	    {
		    $side_mobile_menu_id = wp_create_nav_menu('Side Mobile Menu');
	    }
	    
	    $top_menu_exists = wp_get_nav_menu_object('Top Menu');
	    if(!$top_menu_exists)
	    {
		    $top_menu_id = wp_create_nav_menu('Top Menu');
	    }

		//Check import selected demo
	    if ( class_exists( 'WP_Import' ) ) 
	    { 
	    	switch($_POST['demo'])
	    	{
		    	case 1:
		    	default:
		    		$import_filepath = get_template_directory() ."/cache/demos/1.xml" ;
		    		
		    		$page_on_front = 3602; //Demo Homepage ID
		    		$oldurl = 'http://themes.themegoods.com/grandmagazine/demo';
		    	break;
	    	}
			
			//Run and download demo contents
			$wp_import = new WP_Import();
	        $wp_import->fetch_attachments = true;
	        $wp_import->import($import_filepath);
	        
	        //Remove default Hello World post
	        wp_delete_post(1);
	    }
	    
	    //Set default custom menu settings
	    $locations = get_theme_mod('nav_menu_locations');
		$locations['primary-menu'] = $primary_menu_id;
		$locations['top-menu'] = $top_menu_id;
		$locations['side-menu'] = $side_mobile_menu_id;
		$locations['footer-menu'] = $footer_menu_id;
		set_theme_mod( 'nav_menu_locations', $locations );
		
		//Setup default styling
		$styling_file = get_template_directory() . "/cache/demos/customizer/settings/pink.dat";

		if(file_exists($styling_file))
		{
			WP_Filesystem();
			$wp_filesystem = grandmagazine_get_wp_filesystem();
			$styling_data = $wp_filesystem->get_contents($styling_file);
			$styling_data_arr = unserialize($styling_data);
			
			if(isset($styling_data_arr['mods']) && is_array($styling_data_arr['mods']))
			{	
				// Get menu locations and save to array
				$locations = get_theme_mod('nav_menu_locations');
				$save_menus = array();
				foreach( $locations as $key => $val ) 
				{
					$save_menus[$key] = $val;
				}
			
				//Remove all theme customizer
				remove_theme_mods();
				
				//Re-add the menus
				set_theme_mod('nav_menu_locations', array_map( 'absint', $save_menus ));
			
				foreach($styling_data_arr['mods'] as $key => $styling_mod)
				{
					if(!is_array($styling_mod))
					{
						set_theme_mod( $key, $styling_mod );
					}
				}
			}
		}
		
		//Import widgets
		$import_widget_filepath = get_template_directory() ."/cache/demos/1.wie" ;
		
		// Get file contents and decode
		WP_Filesystem();
		$wp_filesystem = grandmagazine_get_wp_filesystem();
		$data = $wp_filesystem->get_contents($import_widget_filepath);
		$data = json_decode( $data );
	
		// Import the widget data
		// Make results available for display on import/export page
		$widget_import_results = grandmagazine_import_data( $data );
		
		//Change all URLs from demo URL to localhost
		$update_options = array ( 0 => 'content', 1 => 'excerpts', 2 => 'links', 3 => 'attachments', 4 => 'custom', 5 => 'guids', );
		$newurl = esc_url( site_url() ) ;
		grandmagazine_update_urls($update_options, $oldurl, $newurl);
		
		//Refresh rewrite rules
		flush_rewrite_rules();
		
		//Set default Blog featured category
		wp_delete_post( 1, true );
		set_theme_mod('tg_blog_featured_cat', 9);
		set_theme_mod('tg_blog_editor_picks_cat', 7);
	    
		exit();
	}
}*/

/**
*	Setup get styling function
**/
add_action('wp_ajax_grandmagazine_get_styling', 'grandmagazine_get_styling');
add_action('wp_ajax_nopriv_grandmagazine_get_styling', 'grandmagazine_get_styling');

function grandmagazine_get_styling() {
	if(is_admin() && isset($_POST['styling']) && !empty($_POST['styling']))
	{
		$styling_file = get_template_directory() . "/cache/demos/customizer/settings/".$_POST['styling'].".dat";

		if(file_exists($styling_file))
		{
			WP_Filesystem();
			$wp_filesystem = grandmagazine_get_wp_filesystem();
			$styling_data = $wp_filesystem->get_contents($styling_file);
			$styling_data_arr = unserialize($styling_data);
			
			if(isset($styling_data_arr['mods']) && is_array($styling_data_arr['mods']))
			{	
				// Get menu locations and save to array
				$locations = get_theme_mod('nav_menu_locations');
				$save_menus = array();
				foreach( $locations as $key => $val ) 
				{
					$save_menus[$key] = $val;
				}
			
				//Remove all theme customizer
				remove_theme_mods();
				
				//Re-add the menus
				set_theme_mod('nav_menu_locations', array_map( 'absint', $save_menus ));
			
				foreach($styling_data_arr['mods'] as $key => $styling_mod)
				{
					if(!is_array($styling_mod))
					{
						set_theme_mod( $key, $styling_mod );
					}
				}
			}
		    
			exit();
		}
	}
}

/**
*	Setup AJAX search function
**/
add_action('wp_ajax_grandmagazine_ajax_search', 'grandmagazine_ajax_search');
add_action('wp_ajax_nopriv_grandmagazine_ajax_search', 'grandmagazine_ajax_search');

function grandmagazine_ajax_search() 
{
	$wpdb = grandmagazine_get_wpdb();
	
	if (strlen($_POST['s'])>0) {
		$limit=10;
		$s=strtolower(addslashes($_POST['s']));
		$querystr = "
			SELECT $wpdb->posts.*
			FROM $wpdb->posts
			WHERE 1=1 AND ((lower($wpdb->posts.post_title) like %s))
			AND $wpdb->posts.post_type IN ('post', 'page', 'attachment', 'galleries')
			AND (post_status = 'publish')
			ORDER BY $wpdb->posts.post_date DESC
			LIMIT $limit;
		 ";

	 	$pageposts = $wpdb->get_results($wpdb->prepare($querystr, '%'.$wpdb->esc_like($s).'%'), OBJECT);
	 	
	 	if(!empty($pageposts))
	 	{
			echo '<ul>';
	
	 		foreach($pageposts as $result_item) 
	 		{
	 			$post=$result_item;
	 			
	 			$post_type = get_post_type($post->ID);
				$post_type_class = '';
				$post_type_title = '';
				
				$post_thumb = array();
				if(has_post_thumbnail($post->ID, 'thumbnail'))
				{
				    $image_id = get_post_thumbnail_id($post->ID);
				    $post_thumb = wp_get_attachment_image_src($image_id, 'thumbnail', true);
				    $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', true);
				    
				    if(isset($post_thumb[0]) && !empty($post_thumb[0]))
				    {
				        $post_type_class = '<div class="search_thumb"><img src="'.$post_thumb[0].'" alt="'.esc_attr($image_alt).'"/></div>';
				    }
				}
	 			
				echo '<li>';
				echo '<a href="'.get_permalink($post->ID).'">'.$post_type_class.'</i></a>';
				echo '<div class="ajax_post ';
				
				if(!has_post_thumbnail($post->ID, 'thumbnail'))
				{
					echo 'no_post_img';
				}
				
				echo '">';
				echo '<a href="'.get_permalink($post->ID).'"><strong>'.$post->post_title.'</strong><br/>';
				echo '<span class="post_attribute"><i class="fa fa-clock-o"></i>'.date(GRANDMAGAZINE_THEMEDATEFORMAT, strtotime($post->post_date)).'</span></a>';
				echo '</div>';
				echo '</li>';
			}
			
			echo '<li class="view_all"><a class="button" href="javascript:jQuery(\'#searchform\').submit()">'.esc_html__('View all results', 'grandmagazine' ).'</a></li>';
	
			echo '</ul>';
		}

	}
	else 
	{
		echo '';
	}
	die();

}


/**
*	End theme custom AJAX calls handler
**/

add_action('wp_ajax_grandmagazine_script_contact_form', 'grandmagazine_script_contact_form');
add_action('wp_ajax_nopriv_grandmagazine_script_contact_form', 'grandmagazine_script_contact_form');

function grandmagazine_script_contact_form() {
	get_template_part("/modules/script/script-contact-form");

	die();
}

add_action('wp_ajax_grandmagazine_script_custom_css', 'grandmagazine_script_custom_css');
add_action('wp_ajax_nopriv_grandmagazine_script_custom_css', 'grandmagazine_script_custom_css');

function grandmagazine_script_custom_css() {
	get_template_part("/modules/script/script-custom-css");

	die();
}

add_action('wp_ajax_grandmagazine_script_gallery_flexslider', 'grandmagazine_script_gallery_flexslider');
add_action('wp_ajax_nopriv_grandmagazine_script_gallery_flexslider', 'grandmagazine_script_gallery_flexslider');

function grandmagazine_script_gallery_flexslider() {
	get_template_part("/modules/script/script-gallery-flexslider");

	die();
}

add_action('wp_ajax_grandmagazine_script_gridrotator', 'grandmagazine_script_gridrotator');
add_action('wp_ajax_nopriv_grandmagazine_script_gridrotator', 'grandmagazine_script_gridrotator');

function grandmagazine_script_gridrotator() {
	get_template_part("/modules/script/script-gridrotator");

	die();
}

add_action('wp_ajax_grandmagazine_script_self_hosted_video', 'grandmagazine_script_self_hosted_video');
add_action('wp_ajax_nopriv_grandmagazine_script_self_hosted_video', 'grandmagazine_script_self_hosted_video');

function grandmagazine_script_self_hosted_video() {
	get_template_part("/modules/script/script-self-hosted-video");

	die();
}

add_action('wp_ajax_grandmagazine_script_demo', 'grandmagazine_script_demo');
add_action('wp_ajax_nopriv_grandmagazine_script_demo', 'grandmagazine_script_demo');

function grandmagazine_script_demo() {
	get_template_part("/modules/script/script-demo");

	die();
}

add_action('wp_ajax_grandmagazine_get_post_blog_r', 'grandmagazine_get_post_blog_r');
add_action('wp_ajax_nopriv_grandmagazine_get_post_blog_r', 'grandmagazine_get_post_blog_r');

function grandmagazine_get_post_blog_r() {
	$paged = 0;
	if(isset($_POST['paged']))
	{
	    $paged = $_POST['paged'];
	}
	
	$view = 'latest';
	if(isset($_POST['view']))
	{
	    $view = $_POST['view'];
	}
	
	$query_arr = grandmagazine_get_posts_by_view($view, $paged);
	//grandmagazine_debug($query_arr);
	query_posts($query_arr);
	
	if (have_posts()) : while (have_posts()) : the_post();
		$image_thumb = '';
		$image_id = get_post_thumbnail_id(get_the_ID());
?>
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header mixed">
	    	
	    		<?php
				    $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
				    $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				?>
				
				     <div class="post_img static">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
					      		//Get post trending or hot status
						  		echo grandmagazine_get_status(get_the_ID());
					      		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					      	<?php
								//Get post view counter
								$post_view = grandmagazine_get_post_view(get_the_ID());
								
								if(!empty($post_view))
								{
							?>
							<div class="post_info_view">
								<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
							</div>
							<?php
								}
							?>
					     </a>
				     </div>
					 <br class="clear"/>
				<?php
				     }
				?>
	    	
			   <div class="post_header_title">
				   	<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories(get_the_ID());
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
					    ?>
					        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					    <?php
						    	if(GRANDMAGAZINE_THEMEDEMO1)
						    	{
							    	break;
						    	}
						    }
						?>
					</div>
					<?php
						}
					?>
			      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			      	<div class="post_detail post_date">
			      		<span class="post_info_date">
			      			<span>
			      				<a href="<?php the_permalink(); ?>">
				      				<i class="fa fa-clock-o"></i>
				       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      				</a>
			      			</span>
			      		</span>
			      		<span class="post_info_author">
			      			<?php
			      				$author_name = get_the_author();
			      			?>
			      			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><span class="gravatar"><?php echo get_avatar( get_the_author_meta('email'), '60' ); ?></span><?php echo esc_html($author_name); ?></a>
			      		</span>
			      		<span class="post_info_comment">
					  		<i class="fa fa-comment-o"></i><?php echo get_comments_number(get_the_ID()); ?>
					  	</span>
				  	</div>
			   </div>
			      
			    <?php
			      	$tg_blog_display_full = kirki_get_option('tg_blog_display_full');
			      	
			      	if(!empty($tg_blog_display_full))
			      	{
			      		the_content();
			      	}
			      	else
			      	{
			      		echo grandmagazine_get_excerpt_by_id(get_the_ID(), 50);
			      	}
			    ?>
			    
			    <div class="post_button_wrapper">
			    	<a class="readmore" href="<?php the_permalink(); ?>"><?php echo esc_html_e( 'Continue Reading', 'grandmagazine' ); ?></a>
			    </div>
			</div>
			
	    </div>
	    
	</div>

</div>
<!-- End each blog post -->
<?php
	endwhile; endif;

	die();
}

add_action('wp_ajax_grandmagazine_get_post_blog_2cols_r', 'grandmagazine_get_post_blog_2cols_r');
add_action('wp_ajax_nopriv_grandmagazine_get_post_blog_2cols_r', 'grandmagazine_get_post_blog_2cols_r');

function grandmagazine_get_post_blog_2cols_r() {
	$paged = 0;
	if(isset($_POST['paged']))
	{
	    $paged = $_POST['paged'];
	}
	
	$view = 'latest';
	if(isset($_POST['view']))
	{
	    $view = $_POST['view'];
	}
	
	$query_arr = grandmagazine_get_posts_by_view($view, $paged);
	//grandmagazine_debug($query_arr);
	query_posts($query_arr);
	
	$key = 0;
	if (have_posts()) : while (have_posts()) : the_post();
		$image_thumb = '';
		$key++;
		$image_id = get_post_thumbnail_id(get_the_ID());
?>
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php if($key%2==0) { ?>data-column="last"<?php } ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header">
	    	
	    		<?php
				    $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
				    $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				?>
				
				     <div class="post_img static">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
					      		//Get post trending or hot status
						  		echo grandmagazine_get_status(get_the_ID());
						  		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					      	<?php
								//Get post view counter
								$post_view = grandmagazine_get_post_view(get_the_ID());
								
								if(!empty($post_view))
								{
							?>
							<div class="post_info_view">
								<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
							</div>
							<?php
								}
							?>
					      	
					     </a>
				     </div>
					 <br class="clear"/>
				<?php
				     }
				?>
	    	
			   <div class="post_header_title">
				   	<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories(get_the_ID());
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
					    ?>
					        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					    <?php
						    	if(GRANDMAGAZINE_THEMEDEMO)
						    	{
							    	break;
						    	}
						    }
						?>
					</div>
					<?php
						}
					?>
			      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			      	<div class="post_detail post_date">
			      		<span class="post_info_date">
			      			<span>
			      				<a href="<?php the_permalink(); ?>">
				      				<i class="fa fa-clock-o"></i>
				       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      				</a>
			      			</span>
			      		</span>
			      		<span class="post_info_comment">
					  		<i class="fa fa-comment-o"></i><?php echo get_comments_number(get_the_ID()); ?>
					  	</span>
				  	</div>
			   </div>
			      
			    <?php
			      	echo grandmagazine_get_excerpt_by_id(get_the_ID(), 12);
			    ?>
			</div>
			
	    </div>
	    
	</div>

</div>
<!-- End each blog post -->
<?php
	if($key%2==0) 
	{
		echo '<br class="clear"/>';
	}

	endwhile; endif;

	die();
}

add_action('wp_ajax_grandmagazine_get_post_blog_2cols', 'grandmagazine_get_post_blog_2cols');
add_action('wp_ajax_nopriv_grandmagazine_get_post_blog_2cols', 'grandmagazine_get_post_blog_2cols');

function grandmagazine_get_post_blog_2cols() {
	$paged = 0;
	if(isset($_POST['paged']))
	{
	    $paged = $_POST['paged'];
	}
	
	$view = 'latest';
	if(isset($_POST['view']))
	{
	    $view = $_POST['view'];
	}
	
	$query_arr = grandmagazine_get_posts_by_view($view, $paged);
	//grandmagazine_debug($query_arr);
	query_posts($query_arr);
	
	$key = 0;
	if (have_posts()) : while (have_posts()) : the_post();
		$image_thumb = '';
		$key++;
		$image_id = get_post_thumbnail_id(get_the_ID());
?>
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php if($key%2==0) { ?>data-column="last"<?php } ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header mixed">
	    	
	    		<?php
				    $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
				    $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				?>
				
				     <div class="post_img static">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
					      		//Get post trending or hot status
						  		echo grandmagazine_get_status(get_the_ID());
						  		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					      	<?php
								//Get post view counter
								$post_view = grandmagazine_get_post_view(get_the_ID());
								
								if(!empty($post_view))
								{
							?>
							<div class="post_info_view">
								<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
							</div>
							<?php
								}
							?>
					     </a>
				     </div>
					 <br class="clear"/>
				<?php
				     }
				?>
	    	
			   <div class="post_header_title">
				   	<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories(get_the_ID());
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
					    ?>
					        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					    <?php
						    	if(GRANDMAGAZINE_THEMEDEMO)
						    	{
							    	break;
						    	}
						    }
						?>
					</div>
					<?php
						}
					?>
			      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			      	<div class="post_detail post_date">
			      		<span class="post_info_date">
			      			<span>
			      				<a href="<?php the_permalink(); ?>">
				      				<i class="fa fa-clock-o"></i>
				       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      				</a>
			      			</span>
			      		</span>
			      		<span class="post_info_author">
			      			<?php
			      				$author_name = get_the_author();
			      			?>
			      			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><span class="gravatar"><?php echo get_avatar( get_the_author_meta('email'), '60' ); ?></span><?php echo esc_html($author_name); ?></a>
			      		</span>
			      		<span class="post_info_comment">
					  		<i class="fa fa-comment-o"></i><?php echo get_comments_number(get_the_ID()); ?>
					  	</span>
				  	</div>
			   </div>
			      
			    <?php
			      	echo grandmagazine_get_excerpt_by_id(get_the_ID(), 20);
			    ?>
			    
			    <div class="post_button_wrapper">
			    	<a class="readmore" href="<?php the_permalink(); ?>"><?php echo esc_html_e( 'Continue Reading', 'grandmagazine' ); ?></a>
			    </div>
			</div>
			
	    </div>
	    
	</div>

</div>
<!-- End each blog post -->
<?php
	if($key%2==0) 
	{
		echo '<br class="clear"/>';
	}

	endwhile; endif;

	die();
}


add_action('wp_ajax_grandmagazine_get_post_blog_3cols_r', 'grandmagazine_get_post_blog_3cols_r');
add_action('wp_ajax_nopriv_grandmagazine_get_post_blog_3cols_r', 'grandmagazine_get_post_blog_3cols_r');

function grandmagazine_get_post_blog_3cols_r() {
	$paged = 0;
	if(isset($_POST['paged']))
	{
	    $paged = $_POST['paged'];
	}
	
	$view = 'latest';
	if(isset($_POST['view']))
	{
	    $view = $_POST['view'];
	}
	
	$query_arr = grandmagazine_get_posts_by_view($view, $paged);
	//grandmagazine_debug($query_arr);
	query_posts($query_arr);
	
	$key = 0;
	if (have_posts()) : while (have_posts()) : the_post();
		$image_thumb = '';
		$key++;
		$image_id = get_post_thumbnail_id(get_the_ID());
?>
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php if($key%3==0) { ?>data-column="last"<?php } ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header mixed">
	    	
	    		<?php
				    $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
				    $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				?>
				
				     <div class="post_img static">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
					      		//Get post trending or hot status
						  		echo grandmagazine_get_status(get_the_ID());
						  		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					      	<?php
								//Get post view counter
								$post_view = grandmagazine_get_post_view(get_the_ID());
								
								if(!empty($post_view))
								{
							?>
							<div class="post_info_view">
								<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
							</div>
							<?php
								}
							?>
					     </a>
				     </div>
					 <br class="clear"/>
				<?php
				     }
				?>
	    	
			   <div class="post_header_title">
				   	<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories(get_the_ID());
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
					    ?>
					        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					    <?php
						    	if(GRANDMAGAZINE_THEMEDEMO)
						    	{
							    	break;
						    	}
						    }
						?>
					</div>
					<?php
						}
					?>
			      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			      	<div class="post_detail post_date">
			      		<span class="post_info_date">
			      			<span>
			      				<a href="<?php the_permalink(); ?>">
				      				<i class="fa fa-clock-o"></i>
				       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      				</a>
			      			</span>
			      		</span>
			      		<span class="post_info_comment">
					  		<i class="fa fa-comment-o"></i><?php echo get_comments_number(get_the_ID()); ?>
					  	</span>
				  	</div>
			   </div>
			</div>
			
	    </div>
	    
	</div>

</div>
<!-- End each blog post -->
<?php
	if($key%3==0) 
	{
		echo '<br class="clear"/>';
	}

	endwhile; endif;

	die();
}

add_action('wp_ajax_grandmagazine_get_post_blog_3cols', 'grandmagazine_get_post_blog_3cols');
add_action('wp_ajax_nopriv_grandmagazine_get_post_blog_3cols', 'grandmagazine_get_post_blog_3cols');

function grandmagazine_get_post_blog_3cols() {
	$paged = 0;
	if(isset($_POST['paged']))
	{
	    $paged = $_POST['paged'];
	}
	
	$view = 'latest';
	if(isset($_POST['view']))
	{
	    $view = $_POST['view'];
	}
	
	$query_arr = grandmagazine_get_posts_by_view($view, $paged);
	//grandmagazine_debug($query_arr);
	query_posts($query_arr);
	
	$key = 0;
	if (have_posts()) : while (have_posts()) : the_post();
		$image_thumb = '';
		$key++;
		$image_id = get_post_thumbnail_id(get_the_ID());
?>
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php if($key%3==0) { ?>data-column="last"<?php } ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header mixed">
	    	
	    		<?php
				    $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
				    $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				?>
				
				     <div class="post_img static">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
					      		//Get post trending or hot status
						  		echo grandmagazine_get_status(get_the_ID());
						  		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					      	<?php
								//Get post view counter
								$post_view = grandmagazine_get_post_view(get_the_ID());
								
								if(!empty($post_view))
								{
							?>
							<div class="post_info_view">
								<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
							</div>
							<?php
								}
							?>
					     </a>
				     </div>
					 <br class="clear"/>
				<?php
				     }
				?>
	    	
			   <div class="post_header_title">
				   	<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories(get_the_ID());
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
					    ?>
					        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					    <?php
						    	if(GRANDMAGAZINE_THEMEDEMO)
						    	{
							    	break;
						    	}
						    }
						?>
					</div>
					<?php
						}
					?>
			      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			      	<div class="post_detail post_date">
			      		<span class="post_info_date">
			      			<span>
			      				<a href="<?php the_permalink(); ?>">
				      				<i class="fa fa-clock-o"></i>
				       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      				</a>
			      			</span>
			      		</span>
			      		<span class="post_info_author">
			      			<?php
			      				$author_name = get_the_author();
			      			?>
			      			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><span class="gravatar"><?php echo get_avatar( get_the_author_meta('email'), '60' ); ?></span><?php echo esc_html($author_name); ?></a>
			      		</span>
			      		<span class="post_info_comment">
					  		<i class="fa fa-comment-o"></i><?php echo get_comments_number(get_the_ID()); ?>
					  	</span>
				  	</div>
			   </div>
			</div>
			
	    </div>
	    
	</div>

</div>
<!-- End each blog post -->
<?php
	if($key%3==0) 
	{
		echo '<br class="clear"/>';
	}

	endwhile; endif;

	die();
}

add_action('wp_ajax_grandmagazine_get_post_blog_r_list', 'grandmagazine_get_post_blog_r_list');
add_action('wp_ajax_nopriv_grandmagazine_get_post_blog_r_list', 'grandmagazine_get_post_blog_r_list');

function grandmagazine_get_post_blog_r_list() {
	$paged = 0;
	if(isset($_POST['paged']))
	{
	    $paged = $_POST['paged'];
	}
	
	$view = 'latest';
	if(isset($_POST['view']))
	{
	    $view = $_POST['view'];
	}
	
	$query_arr = grandmagazine_get_posts_by_view($view, $paged);
	//grandmagazine_debug($query_arr);
	query_posts($query_arr);
	
	$key = 0;
	if (have_posts()) : while (have_posts()) : the_post();
		$image_thumb = '';
		$key++;
		$image_id = get_post_thumbnail_id(get_the_ID());
?>
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header search">
		    	<?php
				    //Get post featured content
				    $post_content_class = 'one';
				    
				    $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
				    $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				        $post_content_class = 'two_third last';
				?>
				
				    <div class="post_img static one_third">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
					      		//Get post trending or hot status
						  		echo grandmagazine_get_status(get_the_ID());
						  		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					      	<?php
								//Get post view counter
								$post_view = grandmagazine_get_post_view(get_the_ID());
								
								if(!empty($post_view))
								{
							?>
							<div class="post_info_view">
								<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
							</div>
							<?php
								}
							?>
					      	
					     </a>
				     </div>
				
				<?php
				    }
				?>
				
				<div class="post_header_title <?php echo esc_attr($post_content_class); ?>">
					<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories(get_the_ID());
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
					    ?>
					        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					    <?php
						    	if(GRANDMAGAZINE_THEMEDEMO)
						    	{
							    	break;
						    	}
						    }
						?>
					</div>
					<?php
						}
					?>
			      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			      	<div class="post_detail post_date">
			      		<span class="post_info_date">
			      			<span>
			      				<a href="<?php the_permalink(); ?>">
				      				<i class="fa fa-clock-o"></i>
				       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      				</a>
			      			</span>
			      		</span>
			      		<span class="post_info_author">
			      			<?php
			      				$author_name = get_the_author();
			      			?>
			      			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><span class="gravatar"><?php echo get_avatar( get_the_author_meta('email'), '60' ); ?></span><?php echo esc_html($author_name); ?></a>
			      		</span>
			      		<span class="post_info_comment">
					  		<i class="fa fa-comment-o"></i><?php echo get_comments_number(get_the_ID()); ?>
					  	</span>
				  	</div>
				  	<?php echo grandmagazine_get_excerpt_by_id(get_the_ID(), 20); ?>
			   </div>
			</div>
			
	    </div>
	    
	</div>

</div>
<!-- End each blog post -->
<?php
	endwhile; endif;

	die();
}

add_action('wp_ajax_grandmagazine_get_post_blog_f_list', 'grandmagazine_get_post_blog_f_list');
add_action('wp_ajax_nopriv_grandmagazine_get_post_blog_f_list', 'grandmagazine_get_post_blog_f_list');

function grandmagazine_get_post_blog_f_list() {
	$paged = 0;
	if(isset($_POST['paged']))
	{
	    $paged = $_POST['paged'];
	}
	
	$view = 'latest';
	if(isset($_POST['view']))
	{
	    $view = $_POST['view'];
	}
	
	$query_arr = grandmagazine_get_posts_by_view($view, $paged);
	//grandmagazine_debug($query_arr);
	query_posts($query_arr);
	
	$key = 0;
	if (have_posts()) : while (have_posts()) : the_post();
		$image_thumb = '';
		$key++;
		$image_id = get_post_thumbnail_id(get_the_ID());
?>
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header search">
		    	<?php
				    //Get post featured content
				    $post_content_class = 'one';
				    
				    $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
				    $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				        $post_content_class = 'two_third last';
				?>
				
				    <div class="post_img static one_third">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
					      		//Get post trending or hot status
						  		echo grandmagazine_get_status(get_the_ID());
						  		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					      	<?php
								//Get post view counter
								$post_view = grandmagazine_get_post_view(get_the_ID());
								
								if(!empty($post_view))
								{
							?>
							<div class="post_info_view">
								<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
							</div>
							<?php
								}
							?>
					     </a>
				     </div>
				
				<?php
				    }
				?>
				
				<div class="post_header_title <?php echo esc_attr($post_content_class); ?>">
					<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories(get_the_ID());
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
					    ?>
					        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					    <?php
						    	if(GRANDMAGAZINE_THEMEDEMO)
						    	{
							    	break;
						    	}
						    }
						?>
					</div>
					<?php
						}
					?>
			      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			      	<div class="post_detail post_date">
			      		<span class="post_info_date">
			      			<span>
			      				<a href="<?php the_permalink(); ?>">
				      				<i class="fa fa-clock-o"></i>
				       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      				</a>
			      			</span>
			      		</span>
			      		<span class="post_info_author">
			      			<?php
			      				$author_name = get_the_author();
			      			?>
			      			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><span class="gravatar"><?php echo get_avatar( get_the_author_meta('email'), '60' ); ?></span><?php echo esc_html($author_name); ?></a>
			      		</span>
			      		<span class="post_info_comment">
					  		<i class="fa fa-comment-o"></i><?php echo get_comments_number(get_the_ID()); ?>
					  	</span>
				  	</div>
				  	<?php echo grandmagazine_get_excerpt_by_id(get_the_ID(), 45); ?>
			   </div>
			</div>
			
	    </div>
	    
	</div>

</div>
<!-- End each blog post -->
<?php
	endwhile; endif;

	die();
}

add_action('wp_ajax_grandmagazine_get_post_blog_r_grid', 'grandmagazine_get_post_blog_r_grid');
add_action('wp_ajax_nopriv_grandmagazine_get_post_blog_r_grid', 'grandmagazine_get_post_blog_r_grid');

function grandmagazine_get_post_blog_r_grid() {
	$paged = 0;
	if(isset($_POST['paged']))
	{
	    $paged = $_POST['paged'];
	}
	
	$view = 'latest';
	if(isset($_POST['view']))
	{
	    $view = $_POST['view'];
	}
	
	$query_arr = grandmagazine_get_posts_by_view($view, $paged);
	//grandmagazine_debug($query_arr);
	query_posts($query_arr);
	
	$key = 0;
	if (have_posts()) : while (have_posts()) : the_post();
		$image_thumb = '';
		$image_id = get_post_thumbnail_id(get_the_ID());
	
	//Check if first item
	if($key == 0)
	{
?>
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class('first-child'); ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header mixed">
	    	
	    		<?php
				    $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
				    $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				?>
				
				     <div class="post_img static">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
					      		//Get post trending or hot status
						  		echo grandmagazine_get_status(get_the_ID());
						  		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					      	<?php
								//Get post view counter
								$post_view = grandmagazine_get_post_view(get_the_ID());
								
								if(!empty($post_view))
								{
							?>
							<div class="post_info_view">
								<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
							</div>
							<?php
								}
							?>
					     </a>
				     </div>
					 <br class="clear"/>
				<?php
				     }
				?>
	    	
			   	<div class="post_header_title">
				   	<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories(get_the_ID());
					    
					    //Get featured post category
					    $tg_blog_featured_cat = kirki_get_option('tg_blog_featured_cat');
					    
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
					        	
					        	if($cat->term_id != $tg_blog_featured_cat)
					        	{
					    ?>
					        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					    <?php
							    	if(GRANDMAGAZINE_THEMEDEMO)
							    	{
								    	break;
							    	}
						    	}
						    }
						?>
					</div>
					<?php
						}
					?>
			      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			      	<div class="post_detail post_date">
			      		<span class="post_info_date">
			      			<span>
			      				<a href="<?php the_permalink(); ?>">
				      				<i class="fa fa-clock-o"></i>
				       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      				</a>
			      			</span>
			      		</span>
			      		<span class="post_info_author">
			      			<?php
			      				$author_name = get_the_author();
			      			?>
			      			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><span class="gravatar"><?php echo get_avatar( get_the_author_meta('email'), '60' ); ?></span><?php echo esc_html($author_name); ?></a>
			      		</span>
			      		<span class="post_info_comment">
					  		<i class="fa fa-comment-o"></i><?php echo get_comments_number(get_the_ID()); ?>
					  	</span>
				  	</div>
			   </div>
			      
			    <?php
			      	$tg_blog_display_full = kirki_get_option('tg_blog_display_full');
			      	
			      	if(!empty($tg_blog_display_full))
			      	{
			      		the_content();
			      	}
			      	else
			      	{
			      		echo grandmagazine_get_excerpt_by_id(get_the_ID(), 50);
			      	}
			    ?>
			    
			    <div class="post_button_wrapper">
			    	<a class="readmore" href="<?php the_permalink(); ?>"><?php echo esc_html_e( 'Continue Reading', 'grandmagazine' ); ?></a>
			    </div>
			</div>
			
	    </div>
	    
	</div>

</div>
<br class="clear"/>
<!-- End each blog post -->
<?php
} //End if first item
else
{
?>
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php if($key%2==0) { ?>data-column="last"<?php } ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header">
		    	<?php
				    $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
				    $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				?>
				
				   	<div class="post_img static small">
				   	    <a href="<?php the_permalink(); ?>">
				   	    	<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				   	    	
				   	    	<?php
					   	    	//Get post trending or hot status
						  		echo grandmagazine_get_status(get_the_ID());
						  		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					      	<?php
								//Get post view counter
								$post_view = grandmagazine_get_post_view(get_the_ID());
								
								if(!empty($post_view))
								{
							?>
							<div class="post_info_view">
								<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
							</div>
							<?php
								}
							?>
				   	    </a>
				   	</div>
			   <?php
			   		}
			   	?>
			   	<br class="clear"/>
			   	
			   	<div class="post_header_title">
			   		<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories(get_the_ID());
					    
					    //Get featured post category
					    $tg_blog_featured_cat = kirki_get_option('tg_blog_featured_cat');
					    
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
					        	
					        	if($cat->term_id != $tg_blog_featured_cat)
					        	{
					    ?>
					        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					    <?php
							    	if(GRANDMAGAZINE_THEMEDEMO)
							    	{
								    	break;
							    	}
						    	}
						    }
						?>
					</div>
					<?php
						}
					?>
			      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			      	<div class="post_detail post_date">
			      		<span class="post_info_date">
			      			<span>
			      				<a href="<?php the_permalink(); ?>">
				      				<i class="fa fa-clock-o"></i>
				       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      				</a>
			      			</span>
			      		</span>
			      		<span class="post_info_comment">
					  		<i class="fa fa-comment-o"></i><?php echo get_comments_number(get_the_ID()); ?>
					  	</span>
				  	</div>
			   </div>
			      
			    <?php
			      	echo grandmagazine_get_excerpt_by_id(get_the_ID(), 14);
			    ?>
			</div>
			
	    </div>
	    
	</div>

</div>
<!-- End each blog post -->
<?php
}
	//incriment counter
	$key++;

	endwhile; endif;

	die();
}

add_action('wp_ajax_grandmagazine_get_post_blog_f_grid', 'grandmagazine_get_post_blog_f_grid');
add_action('wp_ajax_nopriv_grandmagazine_get_post_blog_f_grid', 'grandmagazine_get_post_blog_f_grid');

function grandmagazine_get_post_blog_f_grid() {
	$paged = 0;
	if(isset($_POST['paged']))
	{
	    $paged = $_POST['paged'];
	}
	
	$view = 'latest';
	if(isset($_POST['view']))
	{
	    $view = $_POST['view'];
	}
	
	$query_arr = grandmagazine_get_posts_by_view($view, $paged);
	//grandmagazine_debug($query_arr);
	query_posts($query_arr);
	
	$key = 0;
	if (have_posts()) : while (have_posts()) : the_post();
		$image_thumb = '';
		$image_id = get_post_thumbnail_id(get_the_ID());
	
	//Check if first item
	if($key == 0)
	{
?>
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class('first-child'); ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header mixed">
	    	
	    		<?php
				    $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
				    $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				?>
				
				     <div class="post_img static">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
					      		//Get post trending or hot status
						  		echo grandmagazine_get_status(get_the_ID());
						  		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					      	<?php
								//Get post view counter
								$post_view = grandmagazine_get_post_view(get_the_ID());
								
								if(!empty($post_view))
								{
							?>
							<div class="post_info_view">
								<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
							</div>
							<?php
								}
							?>
					     </a>
				     </div>
					 <br class="clear"/>
				<?php
				     }
				?>
	    	
			   	<div class="post_header_title">
				   	<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories(get_the_ID());
					    
					    //Get featured post category
					    $tg_blog_featured_cat = kirki_get_option('tg_blog_featured_cat');
					    
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
					        	
					        	if($cat->term_id != $tg_blog_featured_cat)
					        	{
					    ?>
					        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					    <?php
							    	if(GRANDMAGAZINE_THEMEDEMO)
							    	{
								    	break;
							    	}
						    	}
						    }
						?>
					</div>
					<?php
						}
					?>
			      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			      	<div class="post_detail post_date">
			      		<span class="post_info_date">
			      			<span>
			      				<a href="<?php the_permalink(); ?>">
				      				<i class="fa fa-clock-o"></i>
				       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      				</a>
			      			</span>
			      		</span>
			      		<span class="post_info_author">
			      			<?php
			      				$author_name = get_the_author();
			      			?>
			      			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><span class="gravatar"><?php echo get_avatar( get_the_author_meta('email'), '60' ); ?></span><?php echo esc_html($author_name); ?></a>
			      		</span>
			      		<span class="post_info_comment">
					  		<i class="fa fa-comment-o"></i><?php echo get_comments_number(get_the_ID()); ?>
					  	</span>
				  	</div>
			   </div>
			      
			    <?php
			      	$tg_blog_display_full = kirki_get_option('tg_blog_display_full');
			      	
			      	if(!empty($tg_blog_display_full))
			      	{
			      		the_content();
			      	}
			      	else
			      	{
			      		echo grandmagazine_get_excerpt_by_id(get_the_ID(), 50);
			      	}
			    ?>
			    
			    <div class="post_button_wrapper">
			    	<a class="readmore" href="<?php the_permalink(); ?>"><?php echo esc_html_e( 'Continue Reading', 'grandmagazine' ); ?></a>
			    </div>
			</div>
			
	    </div>
	    
	</div>

</div>
<br class="clear"/>
<!-- End each blog post -->
<?php
} //End if first item
else
{
?>
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php if($key%2==0) { ?>data-column="last"<?php } ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header">
		    	<?php
				    $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
				    $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				?>
				
				   	<div class="post_img static small">
				   	    <a href="<?php the_permalink(); ?>">
				   	    	<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				   	    	
				   	    	<?php
					   	    	//Get post trending or hot status
						  		echo grandmagazine_get_status(get_the_ID());
						  		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					      	<?php
								//Get post view counter
								$post_view = grandmagazine_get_post_view(get_the_ID());
								
								if(!empty($post_view))
								{
							?>
							<div class="post_info_view">
								<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
							</div>
							<?php
								}
							?>
				   	    </a>
				   	</div>
			   <?php
			   		}
			   	?>
			   	<br class="clear"/>
			   	
			   	<div class="post_header_title">
			   		<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories(get_the_ID());
					    
					    //Get featured post category
					    $tg_blog_featured_cat = kirki_get_option('tg_blog_featured_cat');
					    
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
					        	
					        	if($cat->term_id != $tg_blog_featured_cat)
					        	{
					    ?>
					        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					    <?php
							    	if(GRANDMAGAZINE_THEMEDEMO)
							    	{
								    	break;
							    	}
						    	}
						    }
						?>
					</div>
					<?php
						}
					?>
			      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			      	<div class="post_detail post_date">
			      		<span class="post_info_date">
			      			<span>
			      				<a href="<?php the_permalink(); ?>">
				      				<i class="fa fa-clock-o"></i>
				       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      				</a>
			      			</span>
			      		</span>
			      		<span class="post_info_comment">
					  		<i class="fa fa-comment-o"></i><?php echo get_comments_number(get_the_ID()); ?>
					  	</span>
				  	</div>
			   </div>
			      
			    <?php
			      	echo grandmagazine_get_excerpt_by_id(get_the_ID(), 14);
			    ?>
			</div>
			
	    </div>
	    
	</div>

</div>
<!-- End each blog post -->
<?php
}
	//incriment counter
	$key++;

	endwhile; endif;

	die();
}

add_action('wp_ajax_grandmagazine_get_post_blog_r_full_list', 'grandmagazine_get_post_blog_r_full_list');
add_action('wp_ajax_nopriv_grandmagazine_get_post_blog_r_full_list', 'grandmagazine_get_post_blog_r_full_list');

function grandmagazine_get_post_blog_r_full_list() {
	$paged = 0;
	if(isset($_POST['paged']))
	{
	    $paged = $_POST['paged'];
	}
	
	$view = 'latest';
	if(isset($_POST['view']))
	{
	    $view = $_POST['view'];
	}
	
	$query_arr = grandmagazine_get_posts_by_view($view, $paged);
	//grandmagazine_debug($query_arr);
	query_posts($query_arr);
	
	$key = 0;
	if (have_posts()) : while (have_posts()) : the_post();
		$image_thumb = '';
		$image_id = get_post_thumbnail_id(get_the_ID());
	
	//Check if first item
	if($key == 0)
	{
?>
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class('first-child'); ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header mixed">
	    	
	    		<?php
				    $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
				    $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				?>
				
				     <div class="post_img static">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
					      		//Get post trending or hot status
						  		echo grandmagazine_get_status(get_the_ID());
						  		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					      	<?php
								//Get post view counter
								$post_view = grandmagazine_get_post_view(get_the_ID());
								
								if(!empty($post_view))
								{
							?>
							<div class="post_info_view">
								<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
							</div>
							<?php
								}
							?>
					     </a>
				     </div>
					 <br class="clear"/>
				<?php
				     }
				?>
	    	
			   	<div class="post_header_title">
				   	<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories(get_the_ID());
					    
					    //Get featured post category
					    $tg_blog_featured_cat = kirki_get_option('tg_blog_featured_cat');
					    
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
					        	
					        	if($cat->term_id != $tg_blog_featured_cat)
					        	{
					    ?>
					        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					    <?php
							    	if(GRANDMAGAZINE_THEMEDEMO)
							    	{
								    	break;
							    	}
						    	}
						    }
						?>
					</div>
					<?php
						}
					?>
			      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			      	<div class="post_detail post_date">
			      		<span class="post_info_date">
			      			<span>
			      				<a href="<?php the_permalink(); ?>">
				      				<i class="fa fa-clock-o"></i>
				       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      				</a>
			      			</span>
			      		</span>
			      		<span class="post_info_author">
			      			<?php
			      				$author_name = get_the_author();
			      			?>
			      			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><span class="gravatar"><?php echo get_avatar( get_the_author_meta('email'), '60' ); ?></span><?php echo esc_html($author_name); ?></a>
			      		</span>
			      		<span class="post_info_comment">
					  		<i class="fa fa-comment-o"></i><?php echo get_comments_number(get_the_ID()); ?>
					  	</span>
				  	</div>
			   </div>
			      
			    <?php
			      	$tg_blog_display_full = kirki_get_option('tg_blog_display_full');
			      	
			      	if(!empty($tg_blog_display_full))
			      	{
			      		the_content();
			      	}
			      	else
			      	{
			      		echo grandmagazine_get_excerpt_by_id(get_the_ID(), 50);
			      	}
			    ?>
			    
			    <div class="post_button_wrapper">
			    	<a class="readmore" href="<?php the_permalink(); ?>"><?php echo esc_html_e( 'Continue Reading', 'grandmagazine' ); ?></a>
			    </div>
			</div>
			
	    </div>
	    
	</div>

</div>
<!-- End each blog post -->
<?php
} //End if first item
else
{
?>
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header search">
		    	<?php
				    //Get post featured content
				    $post_content_class = 'one';
				    
				    $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
				    $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				        $post_content_class = 'two_third last';
				?>
				
				    <div class="post_img static one_third">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
					      		//Get post trending or hot status
						  		echo grandmagazine_get_status(get_the_ID());
						  		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					      	<?php
								//Get post view counter
								$post_view = grandmagazine_get_post_view(get_the_ID());
								
								if(!empty($post_view))
								{
							?>
							<div class="post_info_view">
								<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
							</div>
							<?php
								}
							?>
					     </a>
				     </div>
				
				<?php
				    }
				?>
				
				<div class="post_header_title <?php echo esc_attr($post_content_class); ?>">
					<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories(get_the_ID());
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
					    ?>
					        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					    <?php
						    	if(GRANDMAGAZINE_THEMEDEMO)
						    	{
							    	break;
						    	}
						    }
						?>
					</div>
					<?php
						}
					?>
			      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			      	<div class="post_detail post_date">
			      		<span class="post_info_date">
			      			<span>
			      				<a href="<?php the_permalink(); ?>">
				      				<i class="fa fa-clock-o"></i>
				       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      				</a>
			      			</span>
			      		</span>
			      		<span class="post_info_author">
			      			<?php
			      				$author_name = get_the_author();
			      			?>
			      			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><span class="gravatar"><?php echo get_avatar( get_the_author_meta('email'), '60' ); ?></span><?php echo esc_html($author_name); ?></a>
			      		</span>
			      		<span class="post_info_comment">
					  		<i class="fa fa-comment-o"></i><?php echo get_comments_number(get_the_ID()); ?>
					  	</span>
				  	</div>
				  	<?php echo grandmagazine_get_excerpt_by_id(get_the_ID(), 20); ?>
			   </div>
			</div>
			
	    </div>
	    
	</div>

</div>
<!-- End each blog post -->
<?php
}
	//incriment counter
	$key++;

	endwhile; endif;

	die();
}

add_action('wp_ajax_grandmagazine_get_post_blog_f_full_list', 'grandmagazine_get_post_blog_f_full_list');
add_action('wp_ajax_nopriv_grandmagazine_get_post_blog_f_full_list', 'grandmagazine_get_post_blog_f_full_list');

function grandmagazine_get_post_blog_f_full_list() {
	$paged = 0;
	if(isset($_POST['paged']))
	{
	    $paged = $_POST['paged'];
	}
	
	$view = 'latest';
	if(isset($_POST['view']))
	{
	    $view = $_POST['view'];
	}
	
	$query_arr = grandmagazine_get_posts_by_view($view, $paged);
	//grandmagazine_debug($query_arr);
	query_posts($query_arr);
	
	$key = 0;
	if (have_posts()) : while (have_posts()) : the_post();
		$image_thumb = '';
		$image_id = get_post_thumbnail_id(get_the_ID());
	
	//Check if first item
	if($key == 0)
	{
?>
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class('first-child'); ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header mixed">
	    	
	    		<?php
				    $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
				    $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				?>
				
				     <div class="post_img static">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
					      		//Get post trending or hot status
						  		echo grandmagazine_get_status(get_the_ID());
						  		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					      	<?php
								//Get post view counter
								$post_view = grandmagazine_get_post_view(get_the_ID());
								
								if(!empty($post_view))
								{
							?>
							<div class="post_info_view">
								<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
							</div>
							<?php
								}
							?>
					     </a>
				     </div>
					 <br class="clear"/>
				<?php
				     }
				?>
	    	
			   	<div class="post_header_title">
				   	<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories(get_the_ID());
					    
					    //Get featured post category
					    $tg_blog_featured_cat = kirki_get_option('tg_blog_featured_cat');
					    
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
					        	
					        	if($cat->term_id != $tg_blog_featured_cat)
					        	{
					    ?>
					        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					    <?php
							    	if(GRANDMAGAZINE_THEMEDEMO)
							    	{
								    	break;
							    	}
						    	}
						    }
						?>
					</div>
					<?php
						}
					?>
			      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			      	<div class="post_detail post_date">
			      		<span class="post_info_date">
			      			<span>
			      				<a href="<?php the_permalink(); ?>">
				      				<i class="fa fa-clock-o"></i>
				       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      				</a>
			      			</span>
			      		</span>
			      		<span class="post_info_author">
			      			<?php
			      				$author_name = get_the_author();
			      			?>
			      			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><span class="gravatar"><?php echo get_avatar( get_the_author_meta('email'), '60' ); ?></span><?php echo esc_html($author_name); ?></a>
			      		</span>
			      		<span class="post_info_comment">
					  		<i class="fa fa-comment-o"></i><?php echo get_comments_number(get_the_ID()); ?>
					  	</span>
				  	</div>
			   </div>
			      
			    <?php
			      	$tg_blog_display_full = kirki_get_option('tg_blog_display_full');
			      	
			      	if(!empty($tg_blog_display_full))
			      	{
			      		the_content();
			      	}
			      	else
			      	{
			      		echo grandmagazine_get_excerpt_by_id(get_the_ID(), 60);
			      	}
			    ?>
			    
			    <div class="post_button_wrapper">
			    	<a class="readmore" href="<?php the_permalink(); ?>"><?php echo esc_html_e( 'Continue Reading', 'grandmagazine' ); ?></a>
			    </div>
			</div>
			
	    </div>
	    
	</div>

</div>
<br class="clear"/>
<!-- End each blog post -->
<?php
} //End if first item
else
{
?>
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header search">
		    	<?php
				    //Get post featured content
				    $post_content_class = 'one';
				    
				    $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
				    $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				        $post_content_class = 'two_third last';
				?>
				
				    <div class="post_img static one_third">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
					      		//Get post trending or hot status
						  		echo grandmagazine_get_status(get_the_ID());
						  		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					      	<?php
								//Get post view counter
								$post_view = grandmagazine_get_post_view(get_the_ID());
								
								if(!empty($post_view))
								{
							?>
							<div class="post_info_view">
								<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
							</div>
							<?php
								}
							?>
					     </a>
				     </div>
				
				<?php
				    }
				?>
				
				<div class="post_header_title <?php echo esc_attr($post_content_class); ?>">
					<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories(get_the_ID());
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
					    ?>
					        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					    <?php
						    	if(GRANDMAGAZINE_THEMEDEMO)
						    	{
							    	break;
						    	}
						    }
						?>
					</div>
					<?php
						}
					?>
			      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			      	<div class="post_detail post_date">
			      		<span class="post_info_date">
			      			<span>
			      				<a href="<?php the_permalink(); ?>">
				      				<i class="fa fa-clock-o"></i>
				       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      				</a>
			      			</span>
			      		</span>
			      		<span class="post_info_author">
			      			<?php
			      				$author_name = get_the_author();
			      			?>
			      			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><span class="gravatar"><?php echo get_avatar( get_the_author_meta('email'), '60' ); ?></span><?php echo esc_html($author_name); ?></a>
			      		</span>
			      		<span class="post_info_comment">
					  		<i class="fa fa-comment-o"></i><?php echo get_comments_number(get_the_ID()); ?>
					  	</span>
				  	</div>
				  	<?php echo grandmagazine_get_excerpt_by_id(get_the_ID(), 40); ?>
			   </div>
			</div>
			
	    </div>
	    
	</div>

</div>
<br class="clear"/>
<!-- End each blog post -->
<?php
}
	//incriment counter
	$key++;

	endwhile; endif;

	die();
}

add_action('wp_ajax_grandmagazine_get_post_blog_3cols_newspaper', 'grandmagazine_get_post_blog_3cols_newspaper');
add_action('wp_ajax_nopriv_grandmagazine_get_post_blog_3cols_newspaper', 'grandmagazine_get_post_blog_3cols_newspaper');

function grandmagazine_get_post_blog_3cols_newspaper() {
	$paged = 0;
	if(isset($_POST['paged']))
	{
	    $paged = $_POST['paged'];
	}
	
	$view = 'latest';
	if(isset($_POST['view']))
	{
	    $view = $_POST['view'];
	}
	
	$query_arr = grandmagazine_get_posts_by_view($view, $paged);
	//grandmagazine_debug($query_arr);
	query_posts($query_arr);
	
	$wp_query = grandmagazine_get_wp_query();
    $count_all_posts = $wp_query->post_count;
	
	$key = 0;
	if (have_posts()) : while (have_posts()) : the_post();
		$key++;
		$image_thumb = '';
		$image_id = get_post_thumbnail_id(get_the_ID());
	
	if($key <= 3)
	{
		$classname = 'margintop';
		if($key == 1)
		{
			$classname.= ' first';
		}
?>
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class($classname); ?> <?php if($key%3==0) { ?>data-column="last"<?php } ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header mixed">
	    	
	    		<?php
				    $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
				    $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				?>
				
				     <div class="post_img static">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
					      		//Get post trending or hot status
						  		echo grandmagazine_get_status(get_the_ID());
				    		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					      	<?php
								//Get post view counter
								$post_view = grandmagazine_get_post_view(get_the_ID());
								
								if(!empty($post_view))
								{
							?>
							<div class="post_info_view">
								<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
							</div>
							<?php
								}
							?>
					      	
					     </a>
				     </div>
					 <br class="clear"/>
				<?php
				     }
				?>
	    	
			   <div class="post_header_title">
				   	<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories(get_the_ID());
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
					    ?>
					        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					    <?php
						    	if(GRANDMAGAZINE_THEMEDEMO)
						    	{
							    	break;
						    	}
						    }
						?>
					</div>
					<?php
						}
					?>
			      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			      	<div class="post_detail post_date">
			      		<span class="post_info_date">
			      			<span>
			      				<a href="<?php the_permalink(); ?>">
				      				<i class="fa fa-clock-o"></i>
				       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      				</a>
			      			</span>
			      		</span>
			      		<span class="post_info_author">
			      			<?php
			      				$author_name = get_the_author();
			      			?>
			      			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><span class="gravatar"><?php echo get_avatar( get_the_author_meta('email'), '60' ); ?></span><?php echo esc_html($author_name); ?></a>
			      		</span>
			      		<span class="post_info_comment">
					  		<i class="fa fa-comment-o"></i><?php echo get_comments_number(get_the_ID()); ?>
					  	</span>
				  	</div>
			   </div>
			</div>
			
	    </div>
	    
	</div>

</div>
<!-- End each blog post -->
<?php
		}
		else
		{
?>
<div id="post-<?php the_ID(); ?>" <?php post_class('thumbnail'); ?> <?php if($key%3==0) { ?>data-column="last"<?php } ?>>
	<div class="post_wrapper">
	    <div class="post_content_wrapper">
		    <div class="two_third post_header">
			    <h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			    <div class="post_detail post_date">
			      <span class="post_info_date">
			      	<span>
			      		<a href="<?php the_permalink(); ?>">
				      		<i class="fa fa-clock-o"></i>
				      		<?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      		</a>
			      	</span>
			      	<span class="post_info_comment">
					  	<i class="fa fa-comment-o"></i><?php echo get_comments_number(get_the_ID()); ?>
					</span>
			      </span>
				</div>
		    </div>
		    
		    <div class="one_third last">
			    <?php
					if(has_post_thumbnail(get_the_ID(), 'thumbnail'))
					{
					    $image_id = get_post_thumbnail_id(get_the_ID());
					    $image_thumb = wp_get_attachment_image_src($image_id, 'thumbnail', true);
					}
					
					//Get post featured content
				    if(isset($image_thumb[0]) && !empty($image_thumb[0]))
				    {
				?>
				<div class="post_img thumbnail">
				 	<a href="<?php the_permalink(); ?>">
				 		<img src="<?php echo esc_url($image_thumb[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($image_thumb[1]); ?>px;height:<?php echo esc_attr($image_thumb[2]); ?>px;"/>
				 		
				 		<?php
					 		//Get post trending or hot status
						  	echo grandmagazine_get_status(get_the_ID());
				    	?>
				     	
				    </a>
				</div>
				<?php
					}
				?>
		    </div>
	    </div>
	</div>
</div>
<?php
}

if($key%3==0 OR $key == $count_all_posts) 
{
	echo '<br class="clear"/>';
}
	endwhile; endif;

	die();
}

add_action('wp_ajax_grandmagazine_get_post_blog_newspaper', 'grandmagazine_get_post_blog_newspaper');
add_action('wp_ajax_nopriv_grandmagazine_get_post_blog_newspaper', 'grandmagazine_get_post_blog_newspaper');

function grandmagazine_get_post_blog_newspaper() {
	$paged = 0;
	if(isset($_POST['paged']))
	{
	    $paged = $_POST['paged'];
	}
	
	$view = 'latest';
	if(isset($_POST['view']))
	{
	    $view = $_POST['view'];
	}
	
	$query_arr = grandmagazine_get_posts_by_view($view, $paged);
	//grandmagazine_debug($query_arr);
	query_posts($query_arr);
	
	$wp_query = grandmagazine_get_wp_query();
    $count_all_posts = $wp_query->post_count;
	
	$key = 0;
	
	if (have_posts()) : while (have_posts()) : the_post();
		$key++;
		$image_thumb = '';
		$image_id = get_post_thumbnail_id(get_the_ID());
?>
<div id="post-<?php the_ID(); ?>" <?php post_class('thumbnail'); ?> <?php if($key%3==0) { ?>data-column="last"<?php } ?>>
	<div class="post_wrapper">
	    <div class="post_content_wrapper">
		    <div class="two_third post_header">
			    <h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			    <div class="post_detail post_date">
			      <span class="post_info_date">
			      	<span>
			      		<a href="<?php the_permalink(); ?>">
				      		<i class="fa fa-clock-o"></i>
				      		<?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      		</a>
			      	</span>
			      </span>
			      <span class="post_info_comment">
					  <i class="fa fa-comment-o"></i><?php echo get_comments_number(get_the_ID()); ?>
				  </span>
				</div>
		    </div>
		    
		    <div class="one_third last">
			    <?php
					if(has_post_thumbnail(get_the_ID(), 'thumbnail'))
					{
					    $image_id = get_post_thumbnail_id(get_the_ID());
					    $image_thumb = wp_get_attachment_image_src($image_id, 'thumbnail', true);
				?>
				<div class="post_img thumbnail">
				 	<a href="<?php the_permalink(); ?>">
				 		<img src="<?php echo esc_url($image_thumb[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($image_thumb[1]); ?>px;height:<?php echo esc_attr($image_thumb[2]); ?>px;"/>
				 		
				 		<?php
					 		//Get post trending or hot status
						  	echo grandmagazine_get_status(get_the_ID());
				    	?>
				    </a>
				</div>
				<?php
					}
				?>
		    </div>
	    </div>
	</div>
</div>
<?php
if($key%3==0 OR $key == $count_all_posts) 
{
	echo '<br class="clear"/>';
}

	endwhile; endif;

	die();
}

//Setup custom settings when theme is activated
if (isset($_GET['activated']) && $_GET['activated']){
	//Add default contact fields
	$pp_contact_form = get_option('pp_contact_form');
	if(empty($pp_contact_form))
	{
		add_option( 'pp_contact_form', 's:1:"3";' );
	}
	
	$pp_contact_form_sort_data = get_option('pp_contact_form_sort_data');
	if(empty($pp_contact_form_sort_data))
	{
		add_option( 'pp_contact_form_sort_data', 'a:3:{i:0;s:1:"1";i:1;s:1:"2";i:2;s:1:"3";}' );
	}

	wp_redirect(admin_url("themes.php?page=functions.php&activate=true"));
}
?>